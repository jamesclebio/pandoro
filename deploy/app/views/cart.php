<div class="breadcrumbs">
	<ul>
		<li><a href="<?php echo $this->_url('root'); ?>">Início</a></li>
		<li>Sacola de Compras</li>
	</ul>
</div>

<h2 class="heading-page">Sacola de Compras</h2>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<!-- <div class="box-group">
	<div class="box-message box-message-empty">Sua sacola está vazia. <a href="{% url 'home_loja' %}">Adicione produtos!</a></div>
</div> -->

<div class="box-group">
	<div class="table-responsive">
		<table class="table-products">
			<thead>
				<tr>
					<th class="w-400">Descrição</th>
					<th class="a-center">Entrega/Agendamento</th>
					<th class="a-center">Quantidade</th>
					<th class="a-right">Valor Unitário</th>
					<th class="a-right">Valor Total</th>
					<th class="a-center">Remover</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<a href="#" class="showcase-lite">
							<div class="image"><img src="<?php echo $this->_asset('store/images/products/thumb/sample-1.jpg'); ?>" alt=""></div>
							<div class="description">
								<div class="title">Descrição do produto</div>
								<div class="details">
									<strong>Cor:</strong> Preto<br>
									<strong>Tamanho:</strong> Preto
								</div>
							</div>
						</a>
					</td>
					<td class="a-center">
						<div class="field-delivery">
							<input type="text" value="">
							<!-- <ul class="errorList">
								<li>Dia/horário indisponível.</li>
							</ul> -->
						</div>
					</td>
					<td class="a-center">
						<div class="field-amount" data-action="http://urlbase/">
							<input type="text" value="1">
							<a href="#">Atualizar</a>
						</div>
					</td>
					<td class="a-right">R$ 99,00</td>
					<td class="a-right"><strong>R$ 99,00</strong></td>
					<td class="a-center"><a href="#" class="action-remove">Remover</a></td>
				</tr>
				<tr>
					<td>
						<a href="#" class="showcase-lite">
							<div class="image"><img src="<?php echo $this->_asset('store/images/products/thumb/sample-1.jpg'); ?>" alt=""></div>
							<div class="description">
								<div class="title">Descrição do produto</div>
								<div class="details">
									<strong>Cor:</strong> Preto<br>
									<strong>Tamanho:</strong> Preto
								</div>
							</div>
						</a>
					</td>
					<td class="a-center">
						<div class="field-delivery">
							<input type="text" value="">
							<!-- <ul class="errorList">
								<li>Dia/horário indisponível.</li>
							</ul> -->
						</div>
					</td>
					<td class="a-center">
						<div class="field-amount" data-action="http://urlbase/">
							<input type="text" value="1">
							<a href="#">Atualizar</a>
						</div>
					</td>
					<td class="a-right">R$ 99,00</td>
					<td class="a-right"><strong>R$ 99,00</strong></td>
					<td class="a-center"><a href="#" class="action-remove">Remover</a></td>
				</tr>
				<tr>
					<td>
						<a href="#" class="showcase-lite">
							<div class="image"><img src="<?php echo $this->_asset('store/images/products/thumb/sample-1.jpg'); ?>" alt=""></div>
							<div class="description">
								<div class="title">Descrição do produto</div>
								<div class="details">
									<strong>Cor:</strong> Preto<br>
									<strong>Tamanho:</strong> Preto
								</div>
							</div>
						</a>
					</td>
					<td class="a-center">
						<div class="field-delivery">
							<input type="text" value="">
							<!-- <ul class="errorList">
								<li>Dia/horário indisponível.</li>
							</ul> -->
						</div>
					</td>
					<td class="a-center">
						<div class="field-amount" data-action="http://urlbase/">
							<input type="text" value="1">
							<a href="#">Atualizar</a>
						</div>
					</td>
					<td class="a-right">R$ 99,00</td>
					<td class="a-right"><strong>R$ 99,00</strong></td>
					<td class="a-center"><a href="#" class="action-remove">Remover</a></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="box-group">
	<a href="<?php echo $this->_url('root'); ?>" class="button">Continuar Comprando</a>
	<a href="<?php echo $this->_url('cart'); ?>" class="button">Atualizar Carrinho</a>
</div>

<div class="box-group box-separate">
	<div class="grid grid-items-3">
		<div class="grid-item">
			<h4 class="heading-box">Desconto</h4>
			<form id="form-discount" method="post" action="" class="form-main">
				<fieldset>
					<legend>Desconto</legend>
					<label>Código do cupom *<input name="discount" type="text" required></label>
					<div class="form-action">
						<button type="submit" class="button button-dark">Aplicar Desconto</button>
					</div>
				</fieldset>
			</form>
		</div>

		<div class="grid-item">
			<h4 class="heading-box">Frete</h4>
			<form method="post" action="" class="form-main form-shipping">
				<fieldset>
					<legend>Frete</legend>
					<label>Cidade *
						<select name="delivery-city" data-select-push="[name=delivery-region]" required>
							<option value="" selected disabled>Selecione</option>
							<option value="A" data-push="<?php echo $this->_url('cart/delivery-region/1'); ?>">Option A</option>
							<option value="B" data-push="<?php echo $this->_url('cart/delivery-region/2'); ?>">Option B</option>
							<option value="C" data-push="<?php echo $this->_url('cart/delivery-region/3'); ?>">Option C</option>
						</select>
					</label>
					<label>Região *
						<select name="delivery-region" required>
							<option value="" selected disabled>Selecione</option>
						</select>
					</label>
					<div class="form-action">
						<button type="submit" class="button button-dark">Calcular Frete</button>
					</div>
				</fieldset>
			</form>
		</div>

		<div class="grid-item">
			<h4 class="heading-box">Total</h4>

			<table class="table-total">
				<tr>
					<th>Total em Produtos</th>
					<td>R$ 99,99</td>
				</tr>
				<tr>
					<th>Valor do Frete</th>
					<td>R$ 99,99</td>
				</tr>
				<tr>
					<th>Valor do Desconto</th>
					<td>R$ 99,99</td>
				</tr>
				<tr class="highlight">
					<th>Total</th>
					<td>R$ 99,99</td>
				</tr>
			</table>

			<div class="form-action">
				<a href="#" class="button button-large button-success button-submit">Fechar Compra</a>
			</div>
		</div>
	</div>
</div>
