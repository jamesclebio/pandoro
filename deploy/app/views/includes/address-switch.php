<div class="box-group address-switch" data-action="<?php echo $this->_url('addresses/form/'); ?>">
	<div class="grid grid-items-2">
		<div class="grid-item">
			<h3 class="heading-box">Endereços Cadastrados</h3>

			<ul class="list-buttons">
				<li>
					<a href="#" data-param="1">
						<div class="container">
							<div class="title">James Clébio</div>
							<div class="description">CEP 49050-370, Aracaju-SE</div>
							<div class="note">* Endereço principal</div>
						</div>
					</a>
				</li>
				<li>
					<a href="#" data-param="2">
						<div class="container">
							<div class="title">James Clébio</div>
							<div class="description">CEP 49050-370, Aracaju-SE</div>
						</div>
					</a>
				</li>
				<li>
					<a href="#" data-param="3">
						<div class="container">
							<div class="title">James Clébio</div>
							<div class="description">CEP 49050-370, Aracaju-SE</div>
						</div>
					</a>
				</li>
			</ul>

			<div class="m-top-15 a-right">
				<a href="#" class="button button-dark button-add" data-param="add">Novo endereço de entrega</a>
			</div>
		</div>

		<div class="grid-item"></div>
	</div>
</div>
