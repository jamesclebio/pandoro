<h4>Gnocchi ao Molho Basílico</h4>

<div class="text scroll">
	<h6>Ingredientes</h6>
	<ul>
		<li>100 g de gnocchi</li>
		<li>1 colher (sopa) de cebola picada</li>
		<li>1 colher (sopa) de manteiga ou margarina</li>
		<li>1 dente de alho espremido</li>
		<li>2 tomates maduros, mas firmes, picados, sem pele nem sementes</li>
		<li>5 colheres (sopa) de óleo</li>
		<li>10 folhas de manjericão</li>
		<li>Sal a gosto</li>
	</ul>

	<h6>Modo de Preparo</h6>
	<p>Cozinhe o gnocchi em água e óleo. Escorra e reserve.</p>
	<p>Prepare o molho: refogue a cebola e o alho na manteiga ou margarina e em 2 ½ colheres (sopa) do óleo. Junte em seguida os tomates picados e as folhas de manjericão. Refogue mais um pouco e junte o óleo restante. Junte o gnocchi, misture bem e sirva em seguida.</p>
	
	<h6>Dica</h6>
	<p>Você pode comprar o gnocchi já prontinho na Pandoro. Aí é só fazer o molho ao seu gosto, como o que está acima, por exemplo.</p>
</div>

<a href="#" class="gallery-button">Veja aqui os produtos utilizados nessa receita que você só encontra na Pandoro</a>

<ul class="gallery">
	<li><a href="<?php echo $this->_asset('onepage/images/sample-1.jpg'); ?>" rel="gallery[tip]" title="Title A"></a></li>
	<li><a href="<?php echo $this->_asset('onepage/images/sample-2.jpg'); ?>" rel="gallery[tip]" title="Title B"></a></li>
</ul>

<ul class="nav">
	<li class="previous"><a href="<?php echo $this->_url('tip'); ?>" title="Dica anterior"><span class="icon-chevron-left"></span></a></li>
	<li class="next"><a href="<?php echo $this->_url('tip'); ?>" title="Próxima dica"><span class="icon-chevron-right"></span></a></li>
</ul>
