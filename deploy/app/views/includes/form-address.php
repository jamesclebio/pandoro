<h3 class="heading-box"></h3>

<label>Destinatário *<input name="nome" type="text" required></label>
<label>Telefone *<input name="telefone" type="text" class="mask-phone" required></label>

<div class="box-separate-light">
	<label>CEP *
		<div class="grid grid-items-3">
			<div class="grid-item grid-item-span-2">
				<input name="cep" type="text" class="mask-postal" data-zipcode-check="true" data-delivery-address="<?php echo $this->_url('cart/delivery-address'); ?>" required>
				<div class="field-note">
					Clique em <strong>Validar CEP</strong> para preencher automaticamente Cidade, Estado, Bairro e Endereço.<br>
					<a href="http://www.buscacep.correios.com.br/" target="_blank" class="link-main">Não sabe o CEP?</a>
				</div>
			</div>
			<div class="grid-item">
				<button type="button" class="button-zipcode-check m-top-5 button button-block button-danger">Validar CEP</button>
			</div>
		</div>
	</label>
	<div class="grid grid-items-2">
		<div class="grid-item">
			<label>Cidade *<input name="cidade" type="text" readonly required></label>
		</div>
		<div class="grid-item">
			<label>Estado *<input name="estado" type="text" readonly required></label>
		</div>
	</div>
	<label>Bairro *<input name="bairro" type="text" readonly required></label>
	<label>Endereço *<input name="endereco" type="text" required></label>
	<label>Número *<input name="numero" type="text" required></label>
	<label>Complemento<input name="complemento" type="text"></label>
</div>

<div class="box-separate-light">
	<label class="check"><input name="principal" type="checkbox" value="1">Usar este endereço como principal.</label>
</div>

<div class="box-separate-light">
	<label>Mensagem do cartão *<textarea name="mensagem" cols="30" rows="10" maxlength="100" data-field-count class="h-100" required></textarea></label>
</div>

