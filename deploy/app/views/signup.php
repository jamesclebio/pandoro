<div class="breadcrumbs">
	<ul>
		<li><a href="<?php echo $this->_url('root'); ?>">Início</a></li>
		<li>Cadastre-se</li>
	</ul>
</div>

<h2 class="heading-page">Novo Usuário</h2>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<div class="box-group">
	<form id="form-password" method="post" action="" class="form-main">
		<fieldset>
			<legend>Cadastro</legend>
			<div class="grid grid-items-2">
				<div class="grid-item">
					<h4 class="heading-box">Dados Pessoais</h4>
					<label>Nome completo *<input name="nome" type="text" required></label>
					<label>CPF *<input name="cpf" type="text" class="mask-cpf" required></label>
					<label>RG *<input name="cpf" type="text" required></label>
					<label>E-mail *<input name="email" type="email" required></label>
					<label>Telefone *<input name="telefone" type="text" class="mask-phone" required></label>
					<div class="grid grid-items-2">
						<div class="grid-item">
							<label>Data de Nascimento *<input name="nascimento" type="text" class="mask-date" required></label>
						</div>
						<div class="grid-item">
							<label>Sexo *
								<select name="sexo" required>
									<option value="" selected disabled>Selecione</option>
									<option value="m">Masculino</option>
									<option value="f">Feminino</option>
								</select>
							</label>
						</div>
					</div>
					<label>Nova Senha *<input name="senha" type="password" required></label>
					<label>Confirme a Nova Senha *<input name="senha_conf" type="password" required></label>
				</div>

				<div class="grid-item">
					<h4 class="heading-box">Endereço</h4>
					<label>CEP *
						<div class="grid grid-items-3">
							<div class="grid-item grid-item-span-2">
								<input name="cep" type="text" class="mask-postal" data-zipcode-check="true" required>
								<div class="field-note">
									Clique em <strong>Validar CEP</strong> para preencher automaticamente Cidade, Estado, Bairro e Endereço.<br>
									<a href="http://www.buscacep.correios.com.br/" target="_blank" class="link-main">Não sabe o CEP?</a>
								</div>
							</div>
							<div class="grid-item">
								<button type="button" class="button-zipcode-check m-top-5 button button-block button-danger">Validar CEP</button>
							</div>
						</div>
					</label>
					<div class="grid grid-items-2">
						<div class="grid-item">
							<label>Cidade *<input name="cidade" type="text" required></label>
						</div>
						<div class="grid-item">
							<label>Estado *
								<select name="estado" required>
									<option value="" selected disabled>Selecione</option>
									<option value="AC">Acre</option>
									<option value="AL">Alagoas</option>
									<option value="AP">Amapá</option>
									<option value="AM">Amazonas</option>
									<option value="BA">Bahia</option>
									<option value="CE">Ceará</option>
									<option value="DF">Distrito Federal</option>
									<option value="ES">Espírito Santo</option>
									<option value="GO">Goiás</option>
									<option value="MA">Maranhão</option>
									<option value="MT">Mato Grosso</option>
									<option value="MS">Mato Grosso do Sul</option>
									<option value="MG">Minas Gerais</option>
									<option value="PA">Pará</option>
									<option value="PB">Paraíba</option>
									<option value="PR">Paraná</option>
									<option value="PE">Pernambuco</option>
									<option value="PI">Piauí</option>
									<option value="RJ">Rio de janeiro</option>
									<option value="RN">Rio Grande do Norte</option>
									<option value="RS">Rio Grande do Sul</option>
									<option value="RO">Rondônia</option>
									<option value="RR">Roraima</option>
									<option value="SC">Santa Catarina</option>
									<option value="SP">São Paulo</option>
									<option value="SE">Sergipe</option>
									<option value="TO">Tocantins</option>
								</select>
							</label>
						</div>
					</div>
					<label>Bairro *<input name="bairro" type="text" required></label>
					<label>Endereço *<input name="endereco" type="text" required></label>
					<label>Número *<input name="numero" type="text" required></label>
					<label>Complemento<input name="complemento" type="text"></label>

					<div class="box-separate-light">
						<label class="check"><input name="newsletter" type="checkbox" value="1" checked><strong>Desejo receber por e-mail</strong> novidades da loja.</label>
					</div>
				</div>
			</div>

			<div class="box-terms">
				<h4>Termo de Adesão</h4>

				<h5>1. Lorem ipsum dolor sit amet</h5>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, dolorem, placeat voluptate nemo eum sint ex aliquid reiciendis quaerat asperiores deleniti quasi id obcaecati laborum maiores vel est. Odit, tempora!</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur, neque illo sapiente dolore fugit enim in aspernatur veniam sequi laudantium?</p>

				<h5>2. Lorem ipsum dolor sit amet</h5>
				<ul>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
				</ul>
			</div>
			<label class="check"><input name="termos" type="checkbox" value="1" required><strong>Li e estou de acordo</strong> com as regras descritas no termo de adesão.</label>

			<div class="form-action">
				<button type="submit" class="button">Confirmar</button>
			</div>
		</fieldset>
	</form>
</div>
