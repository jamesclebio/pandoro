<div class="breadcrumbs">
	<ul>
		<li><a href="<?php echo $this->_url('root'); ?>">Início</a></li>
		<li>Alterar Senha</li>
	</ul>
</div>

<h2 class="heading-page">Alterar Senha</h2>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<div class="box-group">
	<form id="form-password" method="post" action="" class="form-main">
		<fieldset>
			<legend>Senha</legend>
			<div class="grid grid-items-2">
				<div class="grid-item">
					<label>Senha Atual *<input name="password_current" type="password" required></label>
					<label>Nova Senha *<input name="password_new" type="password" required></label>
					<label>Confirme a Nova Senha *<input name="password_new_confirm" type="password" required></label>
				</div>
			</div>
			<div class="form-action">
				<button type="submit" class="button">Confirmar</button>
			</div>
		</fieldset>
	</form>
</div>
