<div class="billboard">
	<div class="container">
		<div class="item" style="background-image: url(<?php echo $this->_asset('onepage/images/billboard/nada-nada-nada.png'); ?>);">
			<a href="#url-1"></a>
		</div>

		<div class="item" style="background-image: url(<?php echo $this->_asset('onepage/images/billboard/nada-nada-nada-2.png'); ?>);">
			<a href="#url-2"></a>
		</div>
	</div>
</div>

<div class="global-content">
	<section id="cestas">
		<div class="heading-section">
			<h2>Cestas</h2>
			<a href="<?php echo $this->_url('category'); ?>" class="button button-custom-5">Ver todas as cestas</a>
		</div>

		<div class="showcase-product">
			<div class="product">
				<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
				<div class="price"><span>R$ </span>120,00</div>
				<div class="title">Cesta Felicitá</div>
				<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, harum a vitae pariatur veniam</div>
				<a href="<?php echo $this->_url('product'); ?>" class="buy">Comprar</a>
			</div>
			<div class="product">
				<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
				<div class="price"><span>R$ </span>120,00</div>
				<div class="title">Cesta Felicitá</div>
				<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, harum a vitae pariatur veniam</div>
				<a href="<?php echo $this->_url('product'); ?>" class="buy">Comprar</a>
			</div>
			<div class="product">
				<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
				<div class="price"><span>R$ </span>120,00</div>
				<div class="title">Cesta Felicitá</div>
				<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, harum a vitae pariatur veniam</div>
				<a href="<?php echo $this->_url('product'); ?>" class="buy">Comprar</a>
			</div>
			<div class="product">
				<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
				<div class="price"><span>R$ </span>120,00</div>
				<div class="title">Cesta Felicitá</div>
				<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, harum a vitae pariatur veniam</div>
				<a href="<?php echo $this->_url('product'); ?>" class="buy">Comprar</a>
			</div>
			<div class="product">
				<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
				<div class="price"><span>R$ </span>120,00</div>
				<div class="title">Cesta Felicitá</div>
				<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, harum a vitae pariatur veniam</div>
				<a href="<?php echo $this->_url('product'); ?>" class="buy">Comprar</a>
			</div>
			<div class="product">
				<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
				<div class="price"><span>R$ </span>120,00</div>
				<div class="title">Cesta Felicitá</div>
				<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, harum a vitae pariatur veniam</div>
				<a href="<?php echo $this->_url('product'); ?>" class="buy">Comprar</a>
			</div>
			<div class="product">
				<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
				<div class="price"><span>R$ </span>120,00</div>
				<div class="title">Cesta Felicitá</div>
				<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, harum a vitae pariatur veniam</div>
				<a href="<?php echo $this->_url('product'); ?>" class="buy">Comprar</a>
			</div>
			<div class="product">
				<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
				<div class="price"><span>R$ </span>120,00</div>
				<div class="title">Cesta Felicitá</div>
				<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, harum a vitae pariatur veniam</div>
				<a href="<?php echo $this->_url('product'); ?>" class="buy">Comprar</a>
			</div>
		</div>

		<div class="highlight">
			<h4>Mais Vendidos</h4>
			<div class="showcase-product">
				<div class="product">
					<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
					<div class="price"><span>R$ </span>120,00</div>
					<div class="title">Cesta Felicitá</div>
					<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, harum a vitae pariatur veniam</div>
					<a href="<?php echo $this->_url('product'); ?>" class="buy">Comprar</a>
				</div>
				<div class="product">
					<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
					<div class="price"><span>R$ </span>120,00</div>
					<div class="title">Cesta Felicitá</div>
					<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, harum a vitae pariatur veniam</div>
					<a href="<?php echo $this->_url('product'); ?>" class="buy">Comprar</a>
				</div>
				<div class="product">
					<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
					<div class="price"><span>R$ </span>120,00</div>
					<div class="title">Cesta Felicitá</div>
					<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, harum a vitae pariatur veniam</div>
					<a href="<?php echo $this->_url('product'); ?>" class="buy">Comprar</a>
				</div>
				<div class="product">
					<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
					<div class="price"><span>R$ </span>120,00</div>
					<div class="title">Cesta Felicitá</div>
					<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, harum a vitae pariatur veniam</div>
					<a href="<?php echo $this->_url('product'); ?>" class="buy">Comprar</a>
				</div>
			</div>
		</div>
	</section>

	<section id="a-pandoro">
		<div class="grid grid-items-2">
			<img src="<?php echo $this->_asset('onepage/images/pandoro-loja.png'); ?>" alt="Pandoro">
			<div class="container">
				<h2>A Pandoro</h2>
				<p>Tradição, confiança, sofisticação e excelência no atendimento: são estes os conceitos que definem a Pandoro. Há mais de 8 anos no mercado, a Pandoro oferece um atendimento personalizado, levando aos seus clientes uma variedade de mais de 4 mil itens dos melhores produtos nacionais e importados disponíveis no mercado. São pães, queijos, azeites, vinhos, doces e salgados, além de produtos de fabricação própria que são produzidos artesanalmente com ingredientes de altíssima qualidade.</p>
				<p>Quando se pensa em excelência no atendimento, sofisticação de produtos alimentícios, impossível não lembrar da Pandoro. Um lugar especial para quem procura sempre o melhor.</p>
			</div>
		</div>
	</section>

	<section id="produtos">
		<h2>Produtos</h2>
		<div class="grid grid-items-5">
			<div class="grid-item">
				<h6>Cafeteria</h6>
				<div class="product-icon product-icon-cafeteria"></div>
				<p>A cafeteria da Pandoro serve como ponto de encontro tanto para um bate-papo entre família e amigos como para relaxar e se concentrar na leitura. O cardápio tem como objetivo atender os diversos paladares, com cafés quentes e gelados, shakes, chás, chocolate quente, entre outros.</p>
			</div>
			<div class="grid-item">
				<h6>Padaria & Confeitaria</h6>
				<div class="product-icon product-icon-padaria"></div>
				<p>Utilizando matéria prima de altíssima qualidade, na Pandoro você encontra diversos tipos de pães, tortas, bolos e doces, com receitas tradicionais, criadas especialmente para apreciadores de produtos sofisticados e de excelente qualidade.</p>
			</div>
			<div class="grid-item">
				<h6>Adega</h6>
				<div class="product-icon product-icon-adega"></div>
				<p>O objetivo da Pandoro é criar uma relação inteligente entre qualidade e valor, não se preocupando em grandes variedades e quantidades, mas em oferecer unicamente vinhos de qualidade, reunindo os melhores vinhos disponíveis no mercado.</p>
			</div>
			<div class="grid-item">
				<h6>Frios & Massas</h6>
				<div class="product-icon product-icon-frios"></div>
				<p>A Pandoro trabalha com uma grande variedade de massas e frios nacionais e importados. São diversos tipos de queijos, presuntos, massas e molhos importados e de fabricação própria.</p>
			</div>
			<div class="grid-item">
				<h6>Especiarias</h6>
				<div class="product-icon product-icon-especiarias"></div>
				<p>As especiarias são caracterizadas por interferir no sabor final de um prato. Muitas vezes elas são perceptíveis apenas ao paladar. Sempre tendo em vista a qualidade e diferenciação, a Pandoro trabalha com uma extensa linha de especiarias, nacionais e importadas.</p>
			</div>
		</div>
	</section>

	<section id="dicas-gastronomicas">
		<h2>Dicas Gastronômicas</h2>
		<div class="tip" data-start="<?php echo $this->_url('tip'); ?>"></div>
	</section>

	<section id="contato">
		<ul class="form-switch">
			<li class="active"><a href="#tab-form-contato">Contato</a></li>
			<li><a href="#tab-form-trabalhe">Trabalhe conosco</a></li>
			<li><a href="#tab-form-fornecedor">Seja um fornecedor</a></li>
		</ul>

		<div class="form-switch-container">
			<div id="tab-form-contato">
				<h5>Para tirar dúvidas ou para mais informações, fale com a gente</h5>

				<!-- <div class="alert alert-success">
					<p><strong>Mensagem enviada com sucesso!</strong></p>
				</div> -->

				<div class="grid grid-items-2">
					<div class="grid-item">
						<address>
							Rua Deputado Euclides Paes Mendonça, 105<br>
							Bairro Treze de Julho - CEP 49020-460<br>
							Aracaju-SE<br>
							<a href="tel:07932462600">(79) 3246-2600</a>
						</address>

						<address>
							Pandoro – Shopping Jardins<br>
							Aracaju-SE<br>
							<a href="tel:07932171901">(79) 3217-1901</a>
						</address>
					</div>

					<div class="grid-item">
						<form id="form-contato" method="post" class="form">
							<fieldset>
								<legend>Contato</legend>
								<label><input name="name" type="text" placeholder="Nome" required></label>
								<label><input name="email" type="email" placeholder="E-mail" required></label>
								<label><input name="phone" type="text" placeholder="Telefone" class="mask-phone"></label>
								<label><textarea name="message" cols="30" rows="10" placeholder="Mensagem" class="height-195" required></textarea></label>
								<div class="pane-action">
									<button type="submit" class="button button-large button-custom-2">Enviar</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>

			<div id="tab-form-trabalhe">
				<h5>Se você quer se juntar ao nosso time, envie seu currículo para avaliação</h5>

				<!-- <div class="alert alert-success">
					<p><strong>Mensagem enviada com sucesso!</strong></p>
				</div> -->

				<div class="grid grid-items-2">
					<div class="grid-item">
						<address>
							Rua Deputado Euclides Paes Mendonça, 105<br>
							Bairro Treze de Julho - CEP 49020-460<br>
							Aracaju-SE<br>
							<a href="tel:07932462600">(79) 3246-2600</a>
						</address>

						<address>
							Pandoro – Shopping Jardins<br>
							Aracaju-SE<br>
							<a href="tel:07932171901">(79) 3217-1901</a>
						</address>
					</div>

					<div class="grid-item">
						<form id="form-trabalhe" method="post" class="form">
							<fieldset>
								<legend>Contato</legend>
								<label><input name="name" type="text" placeholder="Nome" required></label>
								<label><input name="email" type="email" placeholder="E-mail" required></label>
								<label><input name="phone" type="text" placeholder="Telefone" class="mask-phone"></label>
								<label>
									<span class="icon-paperclip"></span> <strong>Anexar currículo</strong>
									<input name="curriculo" type="file" required>
									<span class="field-note">Somente <strong>DOC</strong> ou <strong>PDF</strong>, com até <strong>2MB</strong>.</span>
								</label>
								<div class="pane-action">
									<button type="submit" class="button button-large button-custom-2">Enviar</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>

			<div id="tab-form-fornecedor">
				<h5>Seja nosso fornecedor a ajude-nos a manter um rigoroso padrão de qualidade</h5>

				<!-- <div class="alert alert-success">
					<p><strong>Mensagem enviada com sucesso!</strong></p>
				</div> -->

				<div class="grid grid-items-2">
					<div class="grid-item">
						<address>
							Rua Deputado Euclides Paes Mendonça, 105<br>
							Bairro Treze de Julho - CEP 49020-460<br>
							Aracaju-SE<br>
							<a href="tel:07932462600">(79) 3246-2600</a>
						</address>

						<address>
							Pandoro – Shopping Jardins<br>
							Aracaju-SE<br>
							<a href="tel:07932171901">(79) 3217-1901</a>
						</address>
					</div>

					<div class="grid-item">
						<form id="form-fornecedor" method="post" class="form">
							<fieldset>
								<legend>Contato</legend>
								<label><input name="name" type="text" placeholder="Nome" required></label>
								<label><input name="cnpj" type="text" placeholder="CNPJ" class="mask-cnpj"></label>
								<label><input name="email" type="email" placeholder="E-mail" required></label>
								<label><input name="phone" type="text" placeholder="Telefone" class="mask-phone"></label>
								<label><textarea name="message" cols="30" rows="10" placeholder="Mensagem" class="height-195" required></textarea></label>
								<div class="pane-action">
									<button type="submit" class="button button-large button-custom-2">Enviar</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
