<h2 class="heading-page">Acessar Conta</h2>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<div class="box-group">
	<div class="grid grid-items-2">
		<div class="grid-item">
			<h3 class="heading-box">Novo Usuário</h3>

			<div class="box-text">
				<p>Aproveite as vantagens de ser um usuário cadastrado:</p>
				<ul>
					<li>Receba promoções e descontos exclusivos</li>
					<li>Feche pedidos com maior agilidade</li>
					<li>Acompanhe seus pedidos e guarde históricos</li>
				</ul>
			</div>

			<div class="form-action">
				<a href="<?php echo $this->_url('signup'); ?>" class="button button-large">Cadastre-se</a>
			</div>
		</div>

		<div class="grid-item">
			<h3 class="heading-box">Login</h3>

			<form id="form-login" method="post" action="" class="form-main">
				<fieldset>
					<legend>Entrar</legend>
					<label>E-mail *<input name="email" type="text" required></label>
					<label>Senha *<input name="password" type="password" required></label>
					<div class="form-action">
						<ul>
							<li><a href="<?php echo $this->_url('password/remember'); ?>">Esqueceu a senha?</a></li>
						</ul>
						<button type="submit" class="button button-large button-success">Entrar</button>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
