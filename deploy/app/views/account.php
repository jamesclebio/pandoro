<div class="breadcrumbs">
	<ul>
		<li><a href="<?php echo $this->_url('root'); ?>">Início</a></li>
		<li>Meus Dados</li>
	</ul>
</div>

<h2 class="heading-page">Meus Dados</h2>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<div class="box-group">
	<form id="form-password" method="post" action="" class="form-main">
		<fieldset>
			<legend>Cadastro</legend>
			<div class="grid grid-items-2">
				<div class="grid-item">
					<h4 class="heading-box">Dados Pessoais</h4>

					<label>Nome completo *<input name="nome" type="text" readonly></label>
					<label>CPF *<input name="cpf" type="text" class="mask-cpf" readonly></label>
					<label>RG *<input name="cpf" type="text"></label>
					<label>E-mail *<input name="email" type="email" required></label>
					<label>Telefone *<input name="telefone" type="text" class="mask-phone" required></label>
					<div class="grid grid-items-2">
						<div class="grid-item">
							<label>Data de Nascimento *<input name="nascimento" type="text" class="mask-date" required></label>
						</div>
						<div class="grid-item">
							<label>Sexo *
								<select name="sexo" required>
									<option value="m">Masculino</option>
									<option value="f">Feminino</option>
								</select>
							</label>
						</div>
					</div>
					<label class="check"><input name="newsletter" type="checkbox" value="1" checked><strong>Desejo receber por e-mail</strong> novidades da loja.</label>
				</div>
			</div>

			<div class="form-action">
				<button type="submit" class="button">Confirmar</button>
			</div>
		</fieldset>
	</form>
</div>
