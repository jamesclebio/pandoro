<div class="breadcrumbs">
	<ul>
		<li><a href="<?php echo $this->_url('root'); ?>">Início</a></li>
		<li><a href="#">Categoria Pai</a></li>
		<li>Categoria Filho</li>
	</ul>
</div>

<h2 class="heading-page">Categoria Pai</h2>

<div class="grid grid-items-4">
	<div class="grid-item">
		<ul class="list-nav-secondary">
			<li><a href="#">Lorem ipsum dolor</a></li>
			<li class="active"><a href="#">Lorem ipsum dolor</a>
				<div class="box-filter">
					<div class="box-filter-item">
						<h4>Filter Type A</h4>
						<ul class="list-filter">
							<li class="checked"><a href="#">Lorem Ipsum</a></li>
							<li><a href="#">Lorem Ipsum</a></li>
							<li><a href="#">Lorem Ipsum</a></li>
							<li><a href="#">Lorem Ipsum</a></li>
							<li><a href="#">Lorem Ipsum</a></li>
							<li><a href="#">Lorem Ipsum</a></li>
							<li><a href="#">Lorem Ipsum</a></li>
							<li><a href="#">Lorem Ipsum</a></li>
							<li><a href="#">Lorem Ipsum</a></li>
							<li><a href="#">Lorem Ipsum</a></li>
						</ul>
					</div>

					<div class="box-filter-item">
						<h4>Filter Type B</h4>
						<ul class="list-filter">
							<li class="checked"><a href="#">Lorem Ipsum</a></li>
							<li><a href="#">Lorem Ipsum</a></li>
							<li><a href="#">Lorem Ipsum</a></li>
							<li><a href="#">Lorem Ipsum</a></li>
							<li><a href="#">Lorem Ipsum</a></li>
							<li><a href="#">Lorem Ipsum</a></li>
							<li><a href="#">Lorem Ipsum</a></li>
							<li><a href="#">Lorem Ipsum</a></li>
							<li><a href="#">Lorem Ipsum</a></li>
							<li><a href="#">Lorem Ipsum</a></li>
						</ul>
					</div>

					<div class="box-filter-item a-center">
						<a href="#" class="button button-small button-dark">Limpar Filtros</a>
					</div>
				</div>
			</li>
			<li><a href="#">Lorem ipsum dolor</a></li>
			<li><a href="#">Lorem ipsum dolor</a></li>
			<li><a href="#">Lorem ipsum dolor</a></li>
		</ul>
	</div>

	<div class="grid-item grid-item-span-3">
		<div class="box-group">
			<!-- <h3 class="heading-separate"><span>Categoria Filho</span></h3> -->

			<!-- <div class="box-message box-message-empty">Nenhum conteúdo para exibir aqui!</div> -->

			<div class="showcase-product">
				<div class="product">
					<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
					<div class="price"><span>R$ </span>120,00</div>
					<div class="title">Cesta Felicitá</div>
					<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, harum a vitae pariatur veniam</div>
					<a href="<?php echo $this->_url('product'); ?>" class="buy">Comprar</a>
				</div>
				<div class="product">
					<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
					<div class="price"><span>R$ </span>120,00</div>
					<div class="title">Cesta Felicitá</div>
					<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, harum a vitae pariatur veniam</div>
					<a href="<?php echo $this->_url('product'); ?>" class="buy">Comprar</a>
				</div>
				<div class="product">
					<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
					<div class="price"><span>R$ </span>120,00</div>
					<div class="title">Cesta Felicitá</div>
					<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, harum a vitae pariatur veniam</div>
					<a href="<?php echo $this->_url('product'); ?>" class="buy">Comprar</a>
				</div>
				<div class="product">
					<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
					<div class="price"><span>R$ </span>120,00</div>
					<div class="title">Cesta Felicitá</div>
					<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, harum a vitae pariatur veniam</div>
					<a href="<?php echo $this->_url('product'); ?>" class="buy">Comprar</a>
				</div>
				<div class="product">
					<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
					<div class="price"><span>R$ </span>120,00</div>
					<div class="title">Cesta Felicitá</div>
					<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, harum a vitae pariatur veniam</div>
					<a href="<?php echo $this->_url('product'); ?>" class="buy">Comprar</a>
				</div>
				<div class="product">
					<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
					<div class="price"><span>R$ </span>120,00</div>
					<div class="title">Cesta Felicitá</div>
					<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, harum a vitae pariatur veniam</div>
					<a href="<?php echo $this->_url('product'); ?>" class="buy">Comprar</a>
				</div>
			</div>

			<div class="pagination">
				<ul>
					<li class="current"><span>1</span></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><span>...</span></li>
					<li><a href="#">6</a></li>
					<li><a href="#">7</a></li>
					<li><a href="#">8</a></li>
					<li><a href="#">9</a></li>
					<li><a href="#">10</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
