<div class="breadcrumbs">
	<ul>
		<li><a href="<?php echo $this->_url('root'); ?>">Início</a></li>
		<li>Fale Conosco</li>
	</ul>
</div>

<h2 class="heading-page">Fale Conosco</h2>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<div class="box-group">
	<div class="grid grid-items-3">
		<div class="grid-item">
			<div class="heading-box">Endereço</div>

			<ul class="list-display">
				<li>
					<div class="title">Loja Pele</div>
					<div class="description">
						Rua Celso Oliva, 219<br>
						13 de Julho - CEP 49020-090<br>
						Aracaju - Sergipe
					</div>
					<a href="http://goo.gl/maps/PvW0I" target="_blank" class="f-right m-top-5 link-main text-small">Ver mapa</a>
				</li>
			</ul>
		</div>

		<div class="grid-item">
			<div class="heading-box">Atendimento</div>

			<ul class="list-display">
				<li>
					<div class="title">Telefone</div>
					<div class="description">(79) 3211-8889</div>
				</li>
				<li>
					<div class="title">E-mail</div>
					<div class="description"><a href="mailto:sac@vistapele.com.br" class="link-main">sac@vistapele.com.br</a></div>
				</li>
			</ul>
		</div>

		<div class="grid-item">
			<div class="heading-box">Perguntas e Respostas</div>

			<ul class="list-display">
				<li>
					<div class="description">Já deu uma olhada em nossa página de <a href="{% url 'faq' %}" class="link-main">Dúvidas Frequentes</a>? Nela há respostas que talvez possam te ajudar mais rapidamente.</div>
				</li>
			</ul>
		</div>
	</div>
</div>

<div class="box-group box-separate">
	<h4 class="heading-box">Formulário de Contato</h4>

	<form id="form-contact" method="post" action="" class="form-main">
		<fieldset>
			<legend>Contato</legend>
			<div class="grid grid-items-2">
				<div class="grid-item">
					<label>Nome completo *<input name="nome" type="text" required></label>
					<label>E-mail *<input name="email" type="email" required></label>
					<label>Telefone *<input name="telefone" type="text" class="mask-phone" required></label>
					<label class="check"><input name="newsletter" type="checkbox" value="1" checked><strong>Desejo receber por e-mail</strong> novidades da loja.</label>
				</div>

				<div class="grid-item">
					<label>Mensagem *<textarea name="mensagem" id="" cols="30" rows="10" class="h-180"></textarea></label>
				</div>
			</div>

			<div class="form-action">
				<button type="submit" class="button">Confirmar</button>
			</div>
		</fieldset>
	</form>
</div>
