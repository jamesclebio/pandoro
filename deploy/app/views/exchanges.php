<div class="breadcrumbs">
	<ul>
		<li><a href="<?php echo $this->_url('root'); ?>">Início</a></li>
		<li>Trocas e Devoluções</li>
	</ul>
</div>

<h2 class="heading-page">Trocas e Devoluções</h2>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<div class="box-group">
	<div class="box-text">
		<h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit molestiae voluptas</h4>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, expedita, nesciunt sequi suscipit reprehenderit eius. At, ratione dolor esse doloribus et illo mollitia provident quaerat ex consequuntur eum labore in!</p>
		<ul>
			<li>Lorem ipsum dolor sit amet.</li>
			<li>Lorem ipsum dolor sit amet.</li>
			<li>Lorem ipsum dolor sit amet.</li>
		</ul>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium natus at voluptatem perspiciatis veritatis accusamus illo! Laboriosam, nesciunt animi incidunt corrupti harum optio vero explicabo rerum similique rem voluptates blanditiis.</p>
	</div>
</div>
