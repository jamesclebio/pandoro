<div class="breadcrumbs">
	<ul>
		<li><a href="<?php echo $this->_url('root'); ?>">Início</a></li>
		<li>Meus Endereços</li>
	</ul>
</div>

<h2 class="heading-page">Meus Endereços</h2>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<form id="form-address" method="post" action="" class="form-main">
	<fieldset>
		<legend>Endereço</legend>

		<?php include 'address-switch.php' ?>

		<div class="form-action">
				<ul>
					<li><a href="#" class="button-remove">Remover este endereço</a></li>
				</ul>
			<button type="submit" class="button">Confirmar</button>
		</div>
	</fieldset>
</form>
