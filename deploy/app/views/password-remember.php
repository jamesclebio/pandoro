<div class="breadcrumbs">
	<ul>
		<li><a href="<?php echo $this->_url('root'); ?>">Início</a></li>
		<li>Lembrar Senha</li>
	</ul>
</div>

<h2 class="heading-page">Lembrar Senha</h2>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<div class="box-group">
	<form id="form-password" method="post" action="" class="form-main">
		<fieldset>
			<legend>Senha</legend>
			<div class="grid grid-items-2">
				<div class="grid-item">
					<label>E-mail *<input name="email" type="text" required></label>
				</div>
			</div>
			<div class="form-action">
				<button type="submit" class="button">Confirmar</button>
			</div>
		</fieldset>
	</form>
</div>
