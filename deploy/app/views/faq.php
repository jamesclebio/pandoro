<div class="breadcrumbs">
	<ul>
		<li><a href="<?php echo $this->_url('root'); ?>">Início</a></li>
		<li>Dúvidas Frequentes</li>
	</ul>
</div>

<h2 class="heading-page">Dúvidas Frequentes</h2>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<div class="collapse-main collapse-main-open">
	<h4 class="collapse-main-heading"><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias explicabo reiciendis laboriosam qui perspiciatis. Molestias placeat iusto amet libero officia eligendi reprehenderit doloribus odio.</a></h4>

	<div class="collapse-main-container">
		<div class="box-text">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, expedita, nesciunt sequi suscipit reprehenderit eius. At, ratione dolor esse doloribus et illo mollitia provident quaerat ex consequuntur eum labore in!</p>
			<ul>
				<li>Lorem ipsum dolor sit amet.</li>
				<li>Lorem ipsum dolor sit amet.</li>
				<li>Lorem ipsum dolor sit amet.</li>
			</ul>
		</div>
	</div>
</div>

<div class="collapse-main">
	<h4 class="collapse-main-heading"><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic, dolor, animi, porro, molestias deleniti cupiditate autem nobis voluptates fugiat repellendus aliquid ab tenetur itaque.</a></h4>

	<div class="collapse-main-container">
		<div class="box-text">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, expedita, nesciunt sequi suscipit reprehenderit eius. At, ratione dolor esse doloribus et illo mollitia provident quaerat ex consequuntur eum labore in!</p>
			<ul>
				<li>Lorem ipsum dolor sit amet.</li>
				<li>Lorem ipsum dolor sit amet.</li>
				<li>Lorem ipsum dolor sit amet.</li>
			</ul>
		</div>
	</div>
</div>

<div class="collapse-main">
	<h4 class="collapse-main-heading"><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam, sapiente, ratione in animi neque et deserunt molestiae nulla vel accusamus alias excepturi suscipit esse!</a></h4>

	<div class="collapse-main-container">
		<div class="box-text">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, expedita, nesciunt sequi suscipit reprehenderit eius. At, ratione dolor esse doloribus et illo mollitia provident quaerat ex consequuntur eum labore in!</p>
			<ul>
				<li>Lorem ipsum dolor sit amet.</li>
				<li>Lorem ipsum dolor sit amet.</li>
				<li>Lorem ipsum dolor sit amet.</li>
			</ul>
		</div>
	</div>
</div>
