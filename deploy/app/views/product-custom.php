<div class="page-switch">
	<h2>Padaria & Confeitaria</h2>
	<div class="select">
		<h5>Escolha outro produto</h5>
		<ul class="dropdown">
			<li><a href="<?php echo $this->_url('product/custom/1'); ?>">Cafeteria</a></li>
			<li><a href="<?php echo $this->_url('product/custom/2'); ?>">Padaria & Confeitaria</a></li>
			<li><a href="<?php echo $this->_url('product/custom/3'); ?>">Adega</a></li>
			<li><a href="<?php echo $this->_url('product/custom/4'); ?>">Frios & Massas</a></li>
			<li><a href="<?php echo $this->_url('product/custom/5'); ?>">Especiarias</a></li>
		</ul>
	</div>
</div>

<div class="box-group box-heading">
	<p>Utilizando matéria prima de altíssima qualidade, na Pandoro você encontra diversos tipos de pães, tortas, bolos e doces, com receitas tradicionais, criadas especialmente para apreciadores de produtos sofisticados e de excelente qualidade.</p>
</div>

<div class="box-group box-highlight">
	<p>Para encomendar estes produtos entre em contato com a gente: (79) 3246-2600 / 9875-4874</p>
</div>

<div class="box-group">
	<!-- <div class="box-message box-message-empty">Nenhum produto para esta seção!</div> -->
	<div class="showcase-product-custom">
		<a href="<?php echo $this->_asset('store/images/sample-large.jpg'); ?>" rel="gallery[product]" class="product">
			<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
			<div class="title">Cesta Felicitá</div>
			<div class="label-available yes">Disponível</div>
		</a>
		<a href="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" rel="gallery[product]" class="product">
			<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
			<div class="title">Cesta Felicitá</div>
			<div class="label-available yes">Disponível</div>
		</a>
		<a href="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" rel="gallery[product]" class="product">
			<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
			<div class="title">Cesta Felicitá</div>
			<div class="label-available no">Esgotado</div>
		</a>
		<a href="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" rel="gallery[product]" class="product">
			<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
			<div class="title">Cesta Felicitá</div>
			<div class="label-available no">Esgotado</div>
		</a>
		<a href="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" rel="gallery[product]" class="product">
			<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
			<div class="title">Cesta Felicitá</div>
			<div class="label-available no">Esgotado</div>
		</a>
		<a href="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" rel="gallery[product]" class="product">
			<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
			<div class="title">Cesta Felicitá</div>
			<div class="label-available yes">Disponível</div>
		</a>
		<a href="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" rel="gallery[product]" class="product">
			<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
			<div class="title">Cesta Felicitá</div>
			<div class="label-available yes">Disponível</div>
		</a>
		<a href="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" rel="gallery[product]" class="product">
			<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
			<div class="title">Cesta Felicitá</div>
			<div class="label-available no">Esgotado</div>
		</a>
	</div>
</div>

<div class="pagination">
	<ul>
		<li class="current"><span>1</span></li>
		<li><a href="#">2</a></li>
		<li><a href="#">3</a></li>
		<li><a href="#">4</a></li>
		<li><a href="#">5</a></li>
		<li><span>...</span></li>
		<li><a href="#">6</a></li>
		<li><a href="#">7</a></li>
		<li><a href="#">8</a></li>
		<li><a href="#">9</a></li>
		<li><a href="#">10</a></li>
	</ul>
</div>
