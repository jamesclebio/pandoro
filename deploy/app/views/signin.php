<div class="breadcrumbs">
	<ul>
		<li><a href="<?php echo $this->_url('root'); ?>">Início</a></li>
		<li>Entrar</li>
	</ul>
</div>

<h2 class="heading-page">Entrar</h2>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<div class="box-group">
	<form id="form-login" method="post" action="" class="form-main">
		<fieldset>
			<legend>Entrar</legend>
			<div class="grid grid-items-2">
				<div class="grid-item">
					<label>E-mail *<input name="email" type="text" required></label>
				</div>
				<div class="grid-item">
					<label>Senha *<input name="password" type="password" required></label>
				</div>
			</div>
			<div class="form-action">
				<ul>
					<li><a href="<?php echo $this->_url('password/remember'); ?>">Esqueceu a senha?</a></li>
					<li><a href="<?php echo $this->_url('sign/up'); ?>">Não é cadastrado?</a></li>
				</ul>
				<button type="submit" class="button">Entrar</button>
			</div>
		</fieldset>
	</form>
</div>
