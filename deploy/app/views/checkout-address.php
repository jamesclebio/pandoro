<h2 class="heading-page">Endereço de Entrega</h2>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<form id="form-address" method="post" action="" class="form-main">
	<fieldset>
		<legend>Endereço</legend>

		<?php include 'address-switch.php' ?>

		<div class="form-action">
			<button type="submit" class="button button-large button-success">Continuar</button>
		</div>
	</fieldset>
</form>
