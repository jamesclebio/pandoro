<h2 class="heading-page">Confirmar Pagamento</h2>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<div class="box-group">
	<div class="grid grid-items-3">
		<div class="grid-item">
			<h3 class="heading-box">Produto(s)</h3>

			<ul class="list-display">
				<li>
					<div class="title">Lorem ipsum dolor sit</div>
					<div class="description">
						Cor: Preto<br>
						Tamanho: Preto<br>
						Quantidade: 02
					</div>
				</li>
				<li>
					<div class="title">Lorem ipsum dolor sit</div>
					<div class="description">
						Cor: Preto<br>
						Tamanho: Preto<br>
						Quantidade: 02
					</div>
				</li>
			</ul>
		</div>

		<div class="grid-item">
			<h4 class="heading-box">Entrega</h4>

			<ul class="list-display">
				<li>
					<div class="title">James Clébio</div>
					<div class="description">
						Avenida Gonçalo Rolemberg Leite, 999<br>
						Bloco D, Apto 105, Suíssa<br>
						CEP 49000-000, Aracaju-SE
					</div>
					<a href="<?php echo $this->_url('checkout/purchase/step/2'); ?>" class="m-top-5 f-right link-main">Alterar</a>
				</li>
				<li>
					<form class="form-main form-delivery">
						<fieldset>
							<legend>Agendamento</legend>
							<div class="grid grid-items-2">
								<div class="grid-item">
									<label>Dia *<input type="text" name="delivery_day" class="datepicker" data-get-hour="<?php echo $this->_url('cart/delivery-region/1'); ?>" required></label>
								</div>
								<div class="grid-item">
									<label>Horário *
										<select name="delivery_hour" required>
											<option value="" selected disabled>Selecione</option>
											<option value="1">01:00</option>
											<option value="2">02:00</option>
											<option value="3">03:00</option>
										</select>
									</label>
								</div>
							</div>
						</fieldset>
					</form>
				</li>
			</ul>
		</div>

		<div class="grid-item">
			<h3 class="heading-box">Total da Compra</h3>

			<table class="table-total">
				<tr>
					<th>Total em Produtos</th>
					<td>R$ 99,99</td>
				</tr>
				<tr>
					<th>Valor do Frete</th>
					<td>R$ 99,99</td>
				</tr>
				<tr>
					<th>Valor do Desconto</th>
					<td>R$ 99,99</td>
				</tr>
				<tr class="highlight">
					<th>Total</th>
					<td>R$ 99,99</td>
				</tr>
			</table>

			<form method="post" action="<?php echo $this->_url('checkout/pay'); ?>" class="form-main form-payment">
				<input name="delivery_day" type="hidden">
				<input name="delivery_hour" type="hidden">
				<fieldset>
					<legend>Pagamento</legend>

					<div class="alert-main alert-main-info a-right">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae ut magnam recusandae eaque molestias odio.</p>
					</div>

					<div class="form-action">
						<button type="button" class="button button-large button-success button-submit">Efetuar Pagamento</button>
					</div>
				</fieldset>
			</form>

			<div class="m-top-30 a-right">
				<img src="<?php echo $this->_asset('store/images/logo-pagseguro-2.png'); ?>" alt="Logo PagSeguro">
			</div>
		</div>
	</div>
</div>
