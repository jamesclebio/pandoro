<h2 class="heading-page">Confirmação do Pedido</h2>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<div class="box-group">
	<div class="box-well">
		<div class="grid grid-items-3">
			<div class="grid-item a-center">
				<div class="box-stamp box-stamp-success"></div>
			</div>

			<div class="grid-item grid-item-span-2">
				<h3 class="heading-large">Parabéns pela compra, James!</h3>

				<div class="box-text">
					<h4>Seu pedido foi processado com sucesso em nossa loja:</h4>
					<ul>
						<li>Número do pedido: <strong>0123456789</strong></li>
						<li>O pagamento do pedido está sendo analisado pela operadora responsável. Você será notificado por e-mail sobre isso.</li>
					</ul>
					<p>Acompanhe o status de seus pedidos em <a href="<?php echo $this->_url('purchases'); ?>">Meus Pedidos</a>.</p>
					<p>Se tiver dúvidas, consulte nossa página de <a href="<?php echo $this->_url('faq'); ?>">Dúvidas Frequentes</a> ou <a href="<?php echo $this->_url('contact'); ?>">Fale Conosco</a>.</p>
					<p><strong>Obrigado pela preferência! :)</strong></p>
				</div>
			</div>
		</div>
	</div>

	<!-- <div class="box-well">
		<div class="grid grid-items-3">
			<div class="grid-item a-center">
				<div class="box-stamp box-stamp-error"></div>
			</div>

			<div class="grid-item grid-item-span-2">
				<h3 class="heading-large">Ops! Não deu... :(</h3>

				<div class="box-text">
					<h4>Seu pedido não pôde ser processado em nossa loja:</h4>
					<ul>
						<li>Detalhes sobre o problema aqui.</li>
					</ul>
					<p>Se tiver dúvidas, consulte nossa página de <a href="{% url 'faq' %}">Dúvidas Frequentes</a> ou <a href="{% url 'contact' %}">Fale Conosco</a>.</p>
					<p><strong>Obrigado pela preferência! :)</strong></p>
				</div>
			</div>
		</div>
	</div> -->
</div>

<div class="form-action">
	<a href="<?php echo $this->_url('root'); ?>" class="button">Ir para a loja</a>
</div>
