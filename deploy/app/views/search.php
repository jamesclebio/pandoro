<div class="breadcrumbs">
	<ul>
		<li><a href="<?php echo $this->_url('root'); ?>">Início</a></li>
		<li>Busca</li>
	</ul>
</div>

<h2 class="heading-page">Buscando por: termo</h2>

<div class="box-group">
	<h3 class="heading-section">Cestas (99)</h3>
	<!-- <div class="box-message box-message-empty">Nenhum resultado para a busca nesta seção!</div> -->
	<div class="showcase-product">
		<div class="product">
			<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
			<div class="price"><span>R$ </span>120,00</div>
			<div class="title">Cesta Felicitá</div>
			<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, harum a vitae pariatur veniam</div>
			<a href="<?php echo $this->_url('product'); ?>" class="buy">Comprar</a>
		</div>
		<div class="product">
			<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
			<div class="price"><span>R$ </span>120,00</div>
			<div class="title">Cesta Felicitá</div>
			<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, harum a vitae pariatur veniam</div>
			<a href="<?php echo $this->_url('product'); ?>" class="buy">Comprar</a>
		</div>
		<div class="product">
			<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
			<div class="price"><span>R$ </span>120,00</div>
			<div class="title">Cesta Felicitá</div>
			<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, harum a vitae pariatur veniam</div>
			<a href="<?php echo $this->_url('product'); ?>" class="buy">Comprar</a>
		</div>
		<div class="product">
			<div class="image"><img src="<?php echo $this->_asset('onepage/images/products/basket.jpg'); ?>" alt="Product title"></div>
			<div class="price"><span>R$ </span>120,00</div>
			<div class="title">Cesta Felicitá</div>
			<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, harum a vitae pariatur veniam</div>
			<a href="<?php echo $this->_url('product'); ?>" class="buy">Comprar</a>
		</div>
	</div>
</div>

<div class="box-group">
	<h3 class="heading-section">Dicas Gatronômicas (99)</h3>
	<!-- <div class="box-message box-message-empty">Nenhum resultado para a busca nesta seção!</div> -->
	<ul class="list-tips">
		<li>
			<div class="title">Lorem ipsum dolor sit amet, consectetur</div>
			<a href="#" class="view">Ver dica</a>
		</li>
		<li>
			<div class="title">Lorem ipsum dolor sit amet, consectetur</div>
			<a href="#" class="view">Ver dica</a>
		</li>
		<li>
			<div class="title">Lorem ipsum dolor sit amet, consectetur</div>
			<a href="#" class="view">Ver dica</a>
		</li>
		<li>
			<div class="title">Lorem ipsum dolor sit amet, consectetur</div>
			<a href="#" class="view">Ver dica</a>
		</li>
	</ul>
</div>

<div class="pagination">
	<ul>
		<li class="current"><span>1</span></li>
		<li><a href="#">2</a></li>
		<li><a href="#">3</a></li>
		<li><a href="#">4</a></li>
		<li><a href="#">5</a></li>
		<li><span>...</span></li>
		<li><a href="#">6</a></li>
		<li><a href="#">7</a></li>
		<li><a href="#">8</a></li>
		<li><a href="#">9</a></li>
		<li><a href="#">10</a></li>
	</ul>
</div>
