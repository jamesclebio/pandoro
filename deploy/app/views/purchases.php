<div class="breadcrumbs">
	<ul>
		<li><a href="<?php echo $this->_url('root'); ?>">Início</a></li>
		<li>Meus Pedidos</li>
	</ul>
</div>

<h2 class="heading-page">Meus Pedidos</h2>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<div class="box-group box-well box-toggle" data-box-title="Filtro">
	<div class="box-toggle-container">
		<form id="form-filter" method="post" action="" class="form-main">
			<fieldset>
				<legend>Filtro</legend>

				<div class="grid grid-items-4">
					<div class="grid-item">
						<label>Número do pedido<input name="" type="text"></label>
					</div>
					<div class="grid-item">
						<label>Data da compra<input name="" type="text" class="mask-date"></label>
					</div>
					<div class="grid-item">
						<label>Descrição do produto<input name="" type="text"></label>
					</div>
					<div class="grid-item">
						<label>Status
							<select name="">
								<option value="" selected>Todos</option>
							</select>
						</label>
					</div>
				</div>

				<div class="form-action">
					<button type="submit" class="button button-dark">Filtrar</button>
				</div>
			</fieldset>
		</form>
	</div>
</div>

<!-- <div class="box-group">
	<div class="box-message box-message-empty">Nenhum pedido para exibir. <a href="{% url 'home_loja' %}">Compre agora!</a></div>
</div> -->

<div class="box-group">
	<div class="table-responsive">
		<table class="table-products">
			<thead>
				<tr>
					<th>Número</th>
					<th class="a-center">Data/Hora</th>
					<th class="w-450">Descrição</th>
					<th class="w-130 a-center">Status</th>
					<th class="a-right">Valor Total</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="nowrap"><strong>0123456</strong></td>
					<td class="a-center">12/12/1234<br>12:12:12</td>
					<td>
						<a href="#" class="showcase-lite">
							<div class="image"><img src="<?php echo $this->_asset('store/images/products/thumb/sample-1.jpg'); ?>" alt=""></div>
							<div class="description">
								<div class="title">Descrição do produto</div>
								<div class="details">
									<strong>Cor:</strong> Preto<br>
									<strong>Tamanho:</strong> Preto<br>
									<strong>Quantidade:</strong> 02<br>
									<strong>Valor Unitário:</strong> R$ 99,00
								</div>
							</div>
						</a>
					</td>
					<td class="a-center">
						<span class="tag-main">Aguardando Pagamento</span>

						<form method="post" action="ajax/pay.html" class="form-main form-payment m-top-5">
							<fieldset>
								<legend>Pagamento</legend>
								<a href="#" class="link-main button-submit">Efetuar Pagamento</a>
							</fieldset>
						</form>
					</td>
					<td class="a-right"><strong>R$ 99,00</strong></td>
				</tr>
				<tr>
					<td class="nowrap"><strong>0123456</strong></td>
					<td class="a-center">12/12/1234<br>12:12:12</td>
					<td>
						<a href="#" class="showcase-lite">
							<div class="image"><img src="<?php echo $this->_asset('store/images/products/thumb/sample-1.jpg'); ?>" alt=""></div>
							<div class="description">
								<div class="title">Descrição do produto</div>
								<div class="details">
									<strong>Cor:</strong> Preto<br>
									<strong>Tamanho:</strong> Preto<br>
									<strong>Quantidade:</strong> 02<br>
									<strong>Valor Unitário:</strong> R$ 99,00
								</div>
							</div>
						</a>
					</td>
					<td class="a-center">
						<span class="tag-main tag-main-success">Entregue</span>
					</td>
					<td class="a-right"><strong>R$ 99,00</strong></td>
				</tr>
				<tr>
					<td class="nowrap"><strong>0123456</strong></td>
					<td class="a-center">12/12/1234<br>12:12:12</td>
					<td>
						<a href="#" class="showcase-lite">
							<div class="image"><img src="<?php echo $this->_asset('store/images/products/thumb/sample-1.jpg'); ?>" alt=""></div>
							<div class="description">
								<div class="title">Descrição do produto</div>
								<div class="details">
									<strong>Cor:</strong> Preto<br>
									<strong>Tamanho:</strong> Preto<br>
									<strong>Quantidade:</strong> 02<br>
									<strong>Valor Unitário:</strong> R$ 99,00
								</div>
							</div>
						</a>
					</td>
					<td class="a-center">
						<span class="tag-main tag-main-info">Preparando Envio</span>
					</td>
					<td class="a-right"><strong>R$ 99,00</strong></td>
				</tr>
				<tr>
					<td class="nowrap"><strong>0123456</strong></td>
					<td class="a-center">12/12/1234<br>12:12:12</td>
					<td>
						<a href="#" class="showcase-lite">
							<div class="image"><img src="<?php echo $this->_asset('store/images/products/thumb/sample-1.jpg'); ?>" alt=""></div>
							<div class="description">
								<div class="title">Descrição do produto</div>
								<div class="details">
									<strong>Cor:</strong> Preto<br>
									<strong>Tamanho:</strong> Preto<br>
									<strong>Quantidade:</strong> 02<br>
									<strong>Valor Unitário:</strong> R$ 99,00
								</div>
							</div>
						</a>
					</td>
					<td class="a-center">
						<span class="tag-main tag-main-info">Enviado</span>

						<div class="m-top-5">
							<a href="#" class="link-main" target="_blank">Rastrear</a>
						</div>
					</td>
					<td class="a-right"><strong>R$ 99,00</strong></td>
				</tr>
				<tr>
					<td class="nowrap"><strong>0123456</strong></td>
					<td class="a-center">12/12/1234<br>12:12:12</td>
					<td>
						<a href="#" class="showcase-lite">
							<div class="image"><img src="<?php echo $this->_asset('store/images/products/thumb/sample-1.jpg'); ?>" alt=""></div>
							<div class="description">
								<div class="title">Descrição do produto</div>
								<div class="details">
									<strong>Cor:</strong> Preto<br>
									<strong>Tamanho:</strong> Preto<br>
									<strong>Quantidade:</strong> 02<br>
									<strong>Valor Unitário:</strong> R$ 99,00
								</div>
							</div>
						</a>
					</td>
					<td class="a-center">
						<span class="tag-main tag-main-warning">Pagamento em Análise</span>
					</td>
					<td class="a-right"><strong>R$ 99,00</strong></td>
				</tr>
				<tr>
					<td class="nowrap"><strong>0123456</strong></td>
					<td class="a-center">12/12/1234<br>12:12:12</td>
					<td>
						<a href="#" class="showcase-lite">
							<div class="image"><img src="<?php echo $this->_asset('store/images/products/thumb/sample-1.jpg'); ?>" alt=""></div>
							<div class="description">
								<div class="title">Descrição do produto</div>
								<div class="details">
									<strong>Cor:</strong> Preto<br>
									<strong>Tamanho:</strong> Preto<br>
									<strong>Quantidade:</strong> 02<br>
									<strong>Valor Unitário:</strong> R$ 99,00
								</div>
							</div>
						</a>
					</td>
					<td class="a-center">
						<span class="tag-main tag-main-error">Cancelado</span>
					</td>
					<td class="a-right"><strong>R$ 99,00</strong></td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="pagination">
		<ul>
			<li class="current"><span>1</span></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
			<li><span>...</span></li>
			<li><a href="#">6</a></li>
			<li><a href="#">7</a></li>
			<li><a href="#">8</a></li>
			<li><a href="#">9</a></li>
			<li><a href="#">10</a></li>
		</ul>
	</div>
</div>
