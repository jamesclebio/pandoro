<div class="breadcrumbs">
	<ul>
		<li><a href="<?php echo $this->_url('root'); ?>">Início</a></li>
		<li><a href="#">Categoria</a></li>
		<li><a href="#">Subcategoria</a></li>
	</ul>
</div>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<div class="showcase-details">
	<div class="images">
		<div class="medium">
			<img src="<?php echo $this->_asset('store/images/products/medium/sample-1.jpg'); ?>" data-zoom-image="<?php echo $this->_asset('store/images/products/large/sample-1.jpg'); ?>" alt="">
		</div>

		<div class="info">Passe o mouse para dar zoom</div>

		<ul class="list-thumbs">
			<li class="active"><a href="<?php echo $this->_asset('store/images/products/medium/sample-1.jpg'); ?>" data-large="<?php echo $this->_asset('store/images/products/large/sample-1.jpg'); ?>"><img src="<?php echo $this->_asset('store/images/products/thumb/sample-1.jpg'); ?>" alt=""></a></li>
			<li><a href="<?php echo $this->_asset('store/images/products/medium/sample-2.jpg'); ?>" data-large="<?php echo $this->_asset('store/images/products/large/sample-2.jpg'); ?>"><img src="<?php echo $this->_asset('store/images/products/thumb/sample-2.jpg'); ?>" alt=""></a></li>
			<li><a href="<?php echo $this->_asset('store/images/products/medium/sample-1.jpg'); ?>" data-large="<?php echo $this->_asset('store/images/products/large/sample-1.jpg'); ?>"><img src="<?php echo $this->_asset('store/images/products/thumb/sample-1.jpg'); ?>" alt=""></a></li>
			<li><a href="<?php echo $this->_asset('store/images/products/medium/sample-2.jpg'); ?>" data-large="<?php echo $this->_asset('store/images/products/large/sample-2.jpg'); ?>"><img src="<?php echo $this->_asset('store/images/products/thumb/sample-2.jpg'); ?>" alt=""></a></li>
		</ul>
	</div>

	<div class="details">
		<h2>Vestido Seda Painel Tricolor</h2>

		<div class="resume">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat, eaque, architecto molestiae ea consequatur molestias cumque rem eum vel sapiente. Natus, rerum saepe perspiciatis quos optio sint consectetur quia suscipit.</p>
		</div>

		<div class="social">
			<ul class="social-share social-likes_notext">
				<li class="facebook" title="Compartilhar no Facebook"></li>
				<li class="twitter" title="Compartilhar no Twitter"></li>
				<li class="plusone" title="Compartilhar no Google+"></li>
			</ul>
		</div>

		<div class="available"></div>

		<div class="total">
			<div class="price-old">R$ 99,99</div>
			<div class="price">R$ 99,99</div>
		</div>

		<div class="box-group">
			<h5 class="heading-label">Tamanho:</h5>
			<ul class="list-field">
				<li><a href="#" data-value="1" data-stock="8">P</a></li>
				<li><a href="#" data-value="2" data-stock="x">M</a></li>
				<li><a href="#" data-value="3" data-stock="1">G</a></li>
				<li><a href="#" data-value="4" data-stock="0">GG</a></li>
			</ul>
		</div>

		<div class="box-group">
			<h5 class="heading-label">Cores:</h5>
			<ul class="list-thumbs">
				<li><a href="#"><img src="<?php echo $this->_asset('store/images/products/thumb/sample-1.jpg'); ?>" alt=""><span>Azul</span></a></li>
				<li><a href="#"><img src="<?php echo $this->_asset('store/images/products/thumb/sample-2.jpg'); ?>" alt=""><span>Vermelho</span></a></li>
				<li class="active"><a href="#"><img src="<?php echo $this->_asset('store/images/products/thumb/sample-1.jpg'); ?>" alt=""><span>Laranja</span></a></li>
				<li><a href="#"><img src="<?php echo $this->_asset('store/images/products/thumb/sample-1.jpg'); ?>" alt=""><span>Preto</span></a></li>
			</ul>
		</div>

		<div class="action">
			<a href="#" data-action="url/" class="button button-large button-success button-cart">Adicionar à cesta</a>
		</div>

		<div class="description">
			<h4>Detalhes do Produto</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, aliquam odit repellat vero officia reiciendis quis quia nam quidem expedita minus necessitatibus nulla adipisci optio fugit eius aperiam iure sequi!</p>
		</div>
	</div>
</div>
