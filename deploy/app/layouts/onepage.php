<!doctype html>
<html<?php $this->getHtmlAttribute(); ?> class="<?php $this->getHtmlClass(); ?> no-js" lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $this->getTitle(); ?></title>
	<meta name="description" content="<?php echo $this->getDescription(); ?>">
	<link rel="stylesheet" href="<?php echo $this->_asset('onepage/styles/main.css'); ?>">
	<script src="<?php echo $this->_asset('store/scripts/init.js'); ?>"></script>
	<?php $this->getHeadAppend(); ?>
</head>
<body<?php $this->getBodyAttribute(); ?> class="<?php $this->getBodyClass(); ?>">
	<?php $this->getAnalytics(); ?>
	<?php $this->getBodyPrepend(); ?>

	<?php $this->getAlert(); ?>

	<header class="global-header">
		<div class="top">
			<div class="top-container">
				<div class="welcome">Olá, visitante! Faça o <a href="<?php echo $this->_url('sign/in'); ?>">login</a> ou <a href="<?php echo $this->_url('sign/up'); ?>">cadastre-se</a></div>

				<!-- <div class="welcome">Olá, James!</div>
				<ul class="links">
					<li><a href="<?php echo $this->_url('purchases'); ?>">Meus Pedidos</a></li>
					<li><a href="<?php echo $this->_url('addresses'); ?>">Meus Endereços</a></li>
					<li><a href="<?php echo $this->_url('account'); ?>">Meus Dados</a></li>
					<li><a href="<?php echo $this->_url('password/change'); ?>">Alterar Senha</a></li>
					<li><a href="<?php echo $this->_url('sign/out'); ?>">Sair</a></li>
				</ul> -->
			</div>
		</div>

		<div class="middle">
			<div class="middle-container">
				<h1 class="logo"><a href="#!/inicio">Pandoro</a></h1>

				<form id="form-top-search" method="get" action="<?php echo $this->_url('search'); ?>" class="search">
					<fieldset>
						<legend>Busca</legend>
						<input name="q" type="text" required>
						<button type="submit" class="button button-custom-2">Buscar</button>
					</fieldset>
				</form>

				<a href="<?php echo $this->_url('cart'); ?>" class="cart">
					<h4>Minha Cesta</h4>
					<span>(0) itens em sua cesta</span>
				</a>
			</div>
		</div>

		<nav class="nav">
			<ul>
				<li><a href="#!/a-pandoro">A Pandoro</a></li>
				<li><a href="#!/cestas">Cestas</a></li>
				<li><a href="#!/produtos">Produtos</a></li>
				<li><a href="<?php echo $this->_url('product/custom'); ?>">Encomendas</a></li>
				<li><a href="#!/dicas-gastronomicas">Dicas Gastronômicas</a></li>
				<li><a href="<?php echo $this->_url('sign/up'); ?>">Cadastre-se</a></li>
				<li><a href="#!/contato">Contato</a></li>
			</ul>
		</nav>
	</header>

	<?php $this->getView(); ?>

	<footer class="global-footer">
		<div class="legal-message"><strong>* Todas as imagens de produtos mostradas neste site são meramente ilustrativas.</strong></div>

		<div class="grid grid-items-3">
			<div class="grid-item">
				<a href="#!/inicio" class="pandoro">Pandoro</a>
			</div>

			<div class="grid-item align-center">
				<h6 class="margin-top-15 color-custom-3">Acompanhe-nos nas redes sociais</h6>
				<ul class="social">
					<li class="twitter"><a href="https://twitter.com/pandoroaju" target="_blank" title="Twitter">Twitter</a></li>
					<li class="facebook"><a href="https://www.facebook.com/pages/Pandoro/213588828694620" target="_blank" title="Facebook">Facebook</a></li>
					<li class="instagram"><a href="http://instagram.com/pandoroaracaju" target="_blank" title="Instagram">Instagram</a></li>
				</ul>
			</div>

			<div class="grid-item align-right">
				<a href="http://www.agw.com.br" target="_blank" class="agw">AGW</a>
			</div>
		</div>
	</footer>

	<script src="<?php echo $this->_asset('onepage/scripts/main.js'); ?>"></script>
	<?php $this->getBodyAppend(); ?>
</body>
</html>