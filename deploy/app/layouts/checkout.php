<!doctype html>
<html<?php $this->getHtmlAttribute(); ?> class="<?php $this->getHtmlClass(); ?> layout-checkout" lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $this->getTitle(); ?></title>
	<link rel="stylesheet" href="<?php echo $this->_asset('store/styles/main.css'); ?>">
	<script type="text/javascript" src="<?php echo $this->_asset('store/scripts/init.js'); ?>"></script>
	<?php $this->getHeadAppend(); ?>
</head>
<body<?php $this->getBodyAttribute(); ?> class="<?php $this->getBodyClass(); ?>">
	<?php $this->getAnalytics(); ?>
	<?php $this->getBodyPrepend(); ?>

	<header class="global-header">
		<div class="top">
			<div class="top-container">
				<div class="welcome">Olá, visitante! Faça o <a href="<?php echo $this->_url('sign/in'); ?>">login</a> ou <a href="<?php echo $this->_url('sign/up'); ?>">cadastre-se</a></div>

				<!-- <div class="welcome">Olá, James!</div>
				<ul class="links">
					<li><a href="<?php echo $this->_url('purchases'); ?>">Meus pedidos</a></li>
					<li><a href="<?php echo $this->_url('addresses'); ?>">Meus endereços</a></li>
					<li><a href="<?php echo $this->_url('account'); ?>">Meus dados</a></li>
					<li><a href="<?php echo $this->_url('password/change'); ?>">Alterar senha</a></li>
					<li><a href="<?php echo $this->_url('sign/out'); ?>">Sair</a></li>
				</ul> -->
			</div>
		</div>

		<div class="middle">
			<div class="middle-container">
				<h1 class="logo"><a href="<?php echo $this->_url('root'); ?>">Pandoro</a></h1>

				<a href="<?php echo $this->_url('cart'); ?>" class="cart">
					<h4>Minha Cesta</h4>
					<span>(<strong>0</strong>) itens em sua cesta</span>
				</a>
			</div>
		</div>
	</header>

	<div class="checkout-steps checkout-steps-4">
		<div class="container">
			<?php
				switch ($this->_get('step')) {
					case 1:
						echo '<div class="step active">Identificação</div>';
						echo '<div class="step link"><a href="' . $this->_url('checkout/purchase/step/2') . '">Endereço</a></div>';
						echo '<div class="step link"><a href="' . $this->_url('checkout/purchase/step/3') . '">Pagamento</a></div>';
						echo '<div class="step link"><a href="' . $this->_url('checkout/purchase/step/4') . '">Confirmação</a></div>';
						break;

					case 2:
						echo '<div class="step checked">Identificação</div>';
						echo '<div class="step link active"><a href="' . $this->_url('checkout/purchase/step/2') . '">Endereço</a></div>';
						echo '<div class="step link"><a href="' . $this->_url('checkout/purchase/step/3') . '">Pagamento</a></div>';
						echo '<div class="step link"><a href="' . $this->_url('checkout/purchase/step/4') . '">Confirmação</a></div>';
						break;

					case 3:
						echo '<div class="step checked">Identificação</div>';
						echo '<div class="step link checked"><a href="' . $this->_url('checkout/purchase/step/2') . '">Endereço</a></div>';
						echo '<div class="step link active"><a href="' . $this->_url('checkout/purchase/step/3') . '">Pagamento</a></div>';
						echo '<div class="step link"><a href="' . $this->_url('checkout/purchase/step/4') . '">Confirmação</a></div>';
						break;

					case 4:
						echo '<div class="step checked">Identificação</div>';
						echo '<div class="step link checked"><a href="' . $this->_url('checkout/purchase/step/2') . '">Endereço</a></div>';
						echo '<div class="step link checked"><a href="' . $this->_url('checkout/purchase/step/3') . '">Pagamento</a></div>';
						echo '<div class="step link active"><a href="' . $this->_url('checkout/purchase/step/4') . '">Confirmação</a></div>';
						break;
				}
			?>
		</div>
	</div>

	<div class="global-content">
		<?php $this->getView(); ?>
	</div>

	<script type="text/javascript" src="<?php echo $this->_asset('store/scripts/main.js'); ?>"></script>
	<?php $this->getBodyAppend(); ?>
</body>
</html>