<!doctype html>
<html<?php $this->getHtmlAttribute(); ?> class="<?php $this->getHtmlClass(); ?>" lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $this->getTitle(); ?></title>
	<link rel="stylesheet" href="<?php echo $this->_asset('store/styles/main.css'); ?>">
	<script type="text/javascript" src="<?php echo $this->_asset('store/scripts/init.js'); ?>"></script>
	<?php $this->getHeadAppend(); ?>
</head>
<body<?php $this->getBodyAttribute(); ?> class="<?php $this->getBodyClass(); ?>">
	<?php $this->getAnalytics(); ?>
	<?php $this->getBodyPrepend(); ?>

	<a href="javascript:history.back();" class="app-return">Voltar para o aplicativo</a>

	<header class="global-header">
		<div class="top">
			<div class="top-container">
				<div class="welcome">Olá, visitante! Faça o <a href="<?php echo $this->_url('sign/in'); ?>">login</a> ou <a href="<?php echo $this->_url('sign/up'); ?>">cadastre-se</a></div>

				<!-- <div class="welcome">Olá, James!</div>
				<ul class="links">
					<li><a href="<?php echo $this->_url('purchases'); ?>">Meus pedidos</a></li>
					<li><a href="<?php echo $this->_url('addresses'); ?>">Meus endereços</a></li>
					<li><a href="<?php echo $this->_url('account'); ?>">Meus dados</a></li>
					<li><a href="<?php echo $this->_url('password/change'); ?>">Alterar senha</a></li>
					<li><a href="<?php echo $this->_url('sign/out'); ?>">Sair</a></li>
				</ul> -->
			</div>
		</div>

		<div class="middle">
			<div class="middle-container">
				<h1 class="logo"><a href="<?php echo $this->_url('root'); ?>">Pandoro</a></h1>

				<form id="form-top-search" method="get" action="<?php echo $this->_url('search'); ?>" class="search">
					<fieldset>
						<legend>Busca</legend>
						<input name="q" type="text" required>
						<button type="submit" class="button button-custom-2">Buscar</button>
					</fieldset>
				</form>

				<a href="<?php echo $this->_url('cart'); ?>" class="cart">
					<h4>Minha Cesta</h4>
					<span>(<strong>10</strong>) itens em sua cesta</span>
				</a>
			</div>
		</div>

		<nav class="nav">
			<ul>
				<li><a href="<?php echo $this->_url('root'); ?>#!/a-pandoro">A Pandoro</a></li>
				<li><a href="<?php echo $this->_url('root'); ?>#!/cestas">Cestas</a></li>
				<li><a href="<?php echo $this->_url('root'); ?>#!/produtos">Produtos</a></li>
				<li><a href="<?php echo $this->_url('product/custom'); ?>">Encomendas</a></li>
				<li><a href="<?php echo $this->_url('root'); ?>#!/dicas-gastronomicas">Dicas Gastronômicas</a></li>
				<li><a href="<?php echo $this->_url('sign/up'); ?>">Cadastre-se</a></li>
				<li><a href="<?php echo $this->_url('root'); ?>#!/contato">Contato</a></li>
			</ul>
		</nav>
	</header>

	<div class="global-content">
		<?php $this->getView(); ?>

		<div class="legal-message"><strong>* Todas as imagens de produtos mostradas neste site são meramente ilustrativas.</strong></div>
	</div>

	<footer class="global-footer">
		<div class="blocks">
			<div class="grid grid-items-4">
				<div class="grid-item">
					<h4 class="heading-box">Encontre-se</h4>
					<ul class="list-links list-links-light">
						<li><a href="<?php echo $this->_url('purchases'); ?>">Meus Pedidos</a></li>
						<li><a href="<?php echo $this->_url('addresses'); ?>">Meus Endereços</a></li>
						<li><a href="<?php echo $this->_url('account'); ?>">Meus Dados</a></li>
						<li><a href="<?php echo $this->_url('faq'); ?>">Dúvidas Frequentes</a></li>
						<li><a href="<?php echo $this->_url('contact'); ?>">Fale Conosco</a></li>
					</ul>
				</div>

				<div class="grid-item">
					<h4 class="heading-box">Políticas</h4>
					<ul class="list-links list-links-light">
						<li><a href="<?php echo $this->_url('privacy'); ?>">Privacidade e Segurança</a></li>
						<li><a href="<?php echo $this->_url('conditions'); ?>">Termos de Uso</a></li>
						<li><a href="<?php echo $this->_url('exchanges'); ?>">Trocas e Devoluções</a></li>
						<li><a href="<?php echo $this->_url('delivery'); ?>">Processo de Entrega</a></li>
					</ul>
				</div>

				<div class="grid-item">
					<h4 class="heading-box">Segurança</h4>
					<a href="https://pagseguro.uol.com.br/para_voce/protecao_contra_fraudes.jhtml#rmcl" target="_blank"><img src="<?php echo $this->_asset('store/images/logo-pagseguro.png'); ?>" alt="Logo PagSeguro"></a>
				</div>

				<div class="grid-item">
					<h4 class="heading-box">Formas de Pagamento</h4>
					<a href="https://pagseguro.uol.com.br/para_voce/meios_de_pagamento_e_parcelamento.jhtml#rmcl" target="_blank"><img src="<?php echo $this->_asset('store/images/payment.png'); ?>" alt="Formas de Pagamento"></a>
				</div>
			</div>
		</div>

		<div class="copyright">
			<div class="container">
				<a href="<?php echo $this->_url('root'); ?>" class="pandoro">Pandoro</a>

				<address>
					Rua Deputado Euclides Paes Mendonça, 105<br>
					Bairro Treze de Julho - CEP 49020-460<br>
					Aracaju-SE<br>
					<a href="tel:07932462600">(79) 3246-2600</a>
				</address>

				<address>
					Pandoro – Shopping Jardins<br>
					Aracaju-SE<br>
					<a href="tel:07932171901">(79) 3217-1901</a>
				</address>

				<a href="http://www.agw.com.br" target="_blank" class="agw">AGW Internet</a>
			</div>
		</div>
	</footer>

	<script type="text/javascript" src="<?php echo $this->_asset('store/scripts/main.js'); ?>"></script>
	<?php $this->getBodyAppend(); ?>
</body>
</html>