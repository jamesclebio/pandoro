<?php
class Password extends Page
{
	public function __construct() {
		$this->setLayout('store');
		$this->setView(false);
	}

	public function change() {
		$this->setView('password-change');
		$this->setTitle('Pandoro - Alterar Senha');
	}

	public function generate() {
		$this->setView('password-generate');
		$this->setTitle('Pandoro - Nova Senha');
	}

	public function remember() {
		$this->setView('password-remember');
		$this->setTitle('Pandoro - Lembrar Senha');
	}
}
