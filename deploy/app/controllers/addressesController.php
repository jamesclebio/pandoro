<?php
class Addresses extends Page
{
	public function __construct() {
		$this->setLayout('store');
		$this->setView('addresses');
		$this->setTitle('Pandoro - Meus Endereços');
	}

	public function form() {
		$this->setLayout(false);
		$this->setView('includes/form-address');
	}
}
