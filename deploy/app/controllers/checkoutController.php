<?php
class Checkout extends Page
{
	public $step;

	public function __construct() {
		$this->setLayout('checkout');
		$this->setView(false);
	}

	public function purchase() {
		switch ($this->_get('step')) {
			case 1:
				$this->setView('checkout-identification');
				$this->setTitle('Pandoro - Compra - Identificação');
				break;

			case 2:
				$this->setView('checkout-address');
				$this->setTitle('Pandoro - Compra - Endereço');
				break;

			case 3:
				$this->setView('checkout-pay');
				$this->setTitle('Pandoro - Compra - Pagamento');
				break;

			case 4:
				$this->setView('checkout-confirmation');
				$this->setTitle('Pandoro - Compra - Confirmação');
				break;
		}
	}

	public function pay() {
		$this->setLayout(false);
		$this->setView('includes/form-pay');
	}
}
