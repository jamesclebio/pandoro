<?php
class Product extends Page
{
	public function __construct() {
		$this->setLayout('store');
		$this->setView('product');
		$this->setTitle('Pandoro - Produto');
	}

	public function custom() {
		$this->setView('product-custom');
		$this->setTitle('Pandoro - Nossos Produtos');
		$this->setHtmlClass('layout-bgbar');
	}
}
