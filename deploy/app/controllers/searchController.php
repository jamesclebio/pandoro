<?php
class Search extends Page
{
	public function __construct() {
		$this->setLayout('store');
		$this->setView('search');
		$this->setTitle('Pandoro - Busca');
	}
}
