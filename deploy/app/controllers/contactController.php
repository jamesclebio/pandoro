<?php
class Contact extends Page
{
	public function __construct() {
		$this->setLayout('store');
		$this->setView('contact');
		$this->setTitle('Pandoro - Fale Conosco');
	}
}
