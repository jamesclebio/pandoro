<?php
class Purchases extends Page
{
	public function __construct() {
		$this->setLayout('store');
		$this->setView('purchases');
		$this->setTitle('Pandoro - Meus Pedidos');
	}
}
