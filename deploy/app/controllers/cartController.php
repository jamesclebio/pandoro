<?php
class Cart extends Page
{
	public function __construct() {
		$this->setLayout('store');
		$this->setView('cart');
		$this->setTitle('Pandoro - Minha Cesta');
	}

	public function deliveryRegion() {
		$this->setLayout(false);
		$this->setView('includes/delivery-region');
	}

	public function deliveryAddress() {
		$this->setLayout(false);
		$this->setView('includes/delivery-address');
	}
}
