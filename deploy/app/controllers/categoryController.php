<?php
class Category extends Page
{
	public function __construct() {
		$this->setLayout('store');
		$this->setView('category');
		$this->setTitle('Pandoro - Categoria');
	}
}
