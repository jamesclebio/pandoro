<?php
class Sign extends Page
{
	public function __construct() {
		$this->setLayout('store');
		$this->setView(false);
	}

	public function in() {
		$this->setView('signin');
		$this->setTitle('Pandoro - Entrar');
	}

	public function up() {
		$this->setView('signup');
		$this->setTitle('Pandoro - Novo Usuário');
	}

	public function out() {
		$this->setView('signin');
		$this->setTitle('Pandoro - Entrar');
	}
}
