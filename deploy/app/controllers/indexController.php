<?php
class Index extends Page
{
	public function __construct() {
		$this->setLayout('onepage');
		$this->setView('index');
		$this->setTitle('Pandoro');
	}
}
