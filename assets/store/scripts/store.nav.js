;(function($, window, document, undefined) {
	'use strict';

	store.nav = {
		bindings: function() {
			var $wrapper = $('.global-nav');

			$wrapper.find('.nav-level-2 > li > a').on({
				mouseenter: function() {
					var sub = $(this).next('ul');

					if (sub.length && !sub.is(':visible')) {
						$wrapper.find('.nav-level-2 ul').hide();
						$(this).addClass('active');
						sub.clearQueue().fadeIn(0);
					}
				},

				mouseleave: function() {
					var sub = $(this).next('ul');

					if (sub.is(':visible')) {
						sub.delay(10).fadeOut(0, function() {
							sub.prev('a').removeClass('active');
						});
					}
				}
			});

			$wrapper.find('.nav-level-2 ul').on({
				mouseenter: function() {
					$(this).clearQueue();
				},

				mouseleave: function() {
					$(this).prev('a').removeClass('active');
					$(this).fadeOut(0);
				}
			});
		},

		init: function() {
			store.nav.bindings();
		}
	};
}(jQuery, this, this.document));
