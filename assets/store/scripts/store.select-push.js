;(function($, window, document, undefined) {
	'use strict';

	store.selectPush = {
		wrapper: '[data-select-push]',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$wrapper.each(function() {
				var	$target = $($(this).data('selectPush')),
					url = $(this).find(':selected').attr('value');

				if (url) {
					that.hook($target, url);
				}
			});
		},

		hook: function($target, url) {
			var	$zero = $target.find('option:first-child');

			$.ajax({
				type: 'get',
				url: url,

				beforeSend: function() {
					$zero.text('Aguarde...');
				},

				error: function() {
					alert('Ops! Algo deu errado... :(');
				},

				success: function(data) {
					$target.find('option:not(:first-child)').remove();
					$target.append(data);

					if ($target.data('selectPush')) {
						$($target.data('selectPush')).find('option:not(:first-child)').remove();
					}
				},

				complete: function() {
					$zero.text('Selecione');
				}
			});
		},

		bindings: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$wrapper.on({
				change: function() {
					var	$target = $($(this).data('selectPush')),
						url = $(this).find('option[value="' + $(this).val() + '"]').data('push');

					that.hook($target, url);
				}
			});
		},

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.build();
				that.bindings();
			}
		}
	};
}(jQuery, this, this.document));
