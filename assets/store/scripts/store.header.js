;(function($, window, document, undefined) {
	'use strict';

	store.header = {
		bindings: function() {
			var $wrapper = $('.global-header');

			$wrapper.find('.logged-no .login a').on({
				click: function(e) {
					e.preventDefault();
				},

				mouseenter: function() {
					var $sub = $('#form-top-login');

					if ($sub.length && !$sub.is(':visible')) {
						$(this).addClass('active');
						$sub.clearQueue().fadeIn(0);
					}
				},

				mouseleave: function() {
					var	$this = $(this),
						$sub = $('#form-top-login');

					if ($sub.is(':visible')) {
						$sub.delay(10).fadeOut(0, function() {
							$this.removeClass('active');
						});
					}
				}
			});

			$wrapper.find('#form-top-login').on({
				mouseenter: function() {
					$(this).clearQueue();
				},

				mouseleave: function() {
					$wrapper.find('.logged-no .login a').removeClass('active');
					$(this).fadeOut(0);
				}
			});

			$wrapper.find('.logged-yes > ul > li > a').on({
				mouseenter: function() {
					var sub = $(this).next('ul');

					if (sub.length && !sub.is(':visible')) {
						$wrapper.find('ul ul').hide();
						$(this).addClass('active');
						sub.clearQueue().fadeIn(0);
					}
				},

				mouseleave: function() {
					var sub = $(this).next('ul');

					if (sub.is(':visible')) {
						sub.delay(10).fadeOut(0, function() {
							sub.prev('a').removeClass('active');
						});
					}
				}
			});

			$wrapper.find('ul ul').on({
				mouseenter: function() {
					$(this).clearQueue();
				},

				mouseleave: function() {
					$(this).prev('a').removeClass('active');
					$(this).fadeOut(0);
				}
			});
		},

		init: function() {
			store.header.bindings();
		}
	};
}(jQuery, this, this.document));
