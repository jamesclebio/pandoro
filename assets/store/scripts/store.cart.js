;(function($, window, document, undefined) {
	'use strict';

	store.cart = {
		build: function() {
			var	$wrapper = $('.table-products');

			$wrapper.find('.field-amount').each(function() {
				$(this).data('value_current', $(this).find('input').val());
			});
		},

		bindings: function() {
			var	$wrapper = $('.table-products');

			$wrapper.find('.field-amount input').on({
				keydown: function(e) {
					if (e.which == 46 || e.which == 8 || e.which == 9 || e.which == 27 || e.which == 13 || (e.which == 65 && e.ctrlKey === true) || (e.which >= 35 && e.which <= 39))
						return;
					else if (e.shiftKey || (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105 ))
						e.preventDefault();
				},

				keyup: function() {
					var	$link = $(this).next('a'),
						url = $(this).closest('.field-amount').data('action'),
						val_current = $(this).closest('.field-amount').data('value_current'),
						val_input = $(this).val();

					if (isNaN(val_input)) {
						$link.data('url', '');
						$link.hide();
					} else {
						if (val_input != val_current && val_input > 0) {
							$link.data('url', url + val_input + '/');
							$link.show();
						} else {
							$link.data('url', '');
							$link.hide();
						}
					}
				}
			});

			$wrapper.find('.field-amount a').on({
				click: function(e) {
					var	url = $(this).data('url');

					if (url !== '')
						document.location.href = url;

					e.preventDefault();
				}
			});
		},

		init: function() {
			var	$wrapper = $('.table-products');

			if ($wrapper.length) {
				store.cart.build();
				store.cart.bindings();
			}
		}
	};
}(jQuery, this, this.document));
