;(function($, window, document, undefined) {
	'use strict';

	store.collapse = {
		bindings: function() {
			var	$wrapper = $('.collapse-main');

			$wrapper.find('.collapse-main-heading a').on({
				click: function(e) {
					var	$collapse = $(this).closest('.collapse-main'),
						class_open = 'collapse-main-open';

					$collapse.find('.collapse-main-container').fadeToggle(100, function() {
						if ($(this).is(':visible'))
							$collapse.addClass(class_open);
						else
							$collapse.removeClass(class_open);
					});

					e.preventDefault();
				}
			});
		},

		init: function() {
			store.collapse.bindings();
		}
	};
}(jQuery, this, this.document));
