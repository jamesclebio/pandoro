;(function($, window, document, undefined) {
	'use strict';

	store.social = {
		wrapper: '.social-share',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$wrapper.socialLikes({
				zeroes: 'yes'
			});
		},

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.build();
			}
		}
	};
}(jQuery, this, this.document));
