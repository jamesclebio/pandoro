;(function($, window, document, undefined) {
	'use strict';

	store.addressSwitch = {
		build: function() {
			var	$wrapper = $('.address-switch'),
				$form_action = $wrapper.closest('form').find('.form-action'),
				$list_buttons = $wrapper.find('.list-buttons'),
				$active = $list_buttons.find('.active');

			if ($active.length) {
				store.addressSwitch.getAddress($active.find('a'));
			} else {
				store.addressSwitch.getAddress($('.button-add'), true);
			}
		},

		getAddress: function($this, add) {
			var	$wrapper = $('.address-switch'),
				$form_action = $wrapper.closest('form').find('.form-action'),
				$button_remove = $form_action.find('.button-remove').closest('li'),
				$list_buttons = $wrapper.find('.list-buttons'),
				$target = $wrapper.find('.grid-item:last-child'),
				url = $wrapper.data('action') + $this.data('param');

			$.ajax({
				url: url,

				beforeSend: function() {
					$this.closest('li').addClass('loading');
				},

				error: function() {
					alert('Ops! Algo deu errado... Tente novamente.');
				},

				success: function(data) {
					$target.html(data);

					$list_buttons.find('li').removeClass('active');

					if (add) {
						$target.find('.heading-box').text('Endereço de entrega');
						$target.find('label:first input').focus();

						if ($button_remove.length)
							$button_remove.hide();
					} else {
						$target.find('.heading-box').text('Endereço de entrega selecionado');
						$this.closest('li').addClass('active');

						if ($button_remove.length)
							$button_remove.show();
					}

					if (!$form_action.is(':visible'))
						$form_action.show();

					store.init('form', 'selectPush', 'fieldCount');
				},

				complete: function() {
					$this.closest('li').removeClass('loading');
				}
			});
		},

		validate: function($form) {
			var	$target = $form.find('.grid-item:last-child'),
				$button_submit = $form.find('[type="submit"]'),
				text_button_submit = $button_submit.text();

			function resultError() {
				var	$alert = $('.alert-main'),
					$field_error = $form.find('.errorList'),
					template_alert = '<div class="alert-main alert-main-error"><a href="#" class="close" title="Fechar alerta">x</a><p><strong>Ops! Algo deu errado... :(</strong></p><p>Por favor, tente novamente. Se o problema persistir, tente <a href="#">falar conosco</a> relatando este ocorrido. Teremos prazer em ajudar!</p></div>';

				$form.removeData('checked');
				$button_submit.removeAttr('disabled').text(text_button_submit);

				if ($alert.length) {
					$alert.fadeOut(100, function() {
						$(this).remove();

						if (!$field_error.length)
							$('.heading-page').after(template_alert);
					});
				} else if (!$field_error.length)
					$('.heading-page').after(template_alert);

				$('html, body').animate({
					scrollTop: $('.heading-page').offset().top - 15
				}, 150);
			}

			$.ajax({
				cache: false,
				type: $form.attr('method'),
				url: $form.attr('action'),
				data: $form.serialize(),

				beforeSend: function() {
					$button_submit.attr('disabled', 'disabled').text('Validando Endereço...');
				},

				error: function() {
					resultError();
				},

				success: function(data) {
					var	$action,
						text_heading = $target.find('.heading-box').text();

					$target.empty().append(data).find('.heading-box').text(text_heading);
					$action = $form.find('[name="form-action"]');

					store.form.masks();
					store.form.bindings();

					if ($action.val() === '' || $action.val() === undefined)
						resultError();
					else {
						$form.attr('action', $action.val()).data('checked', true);
						$action.remove();
						$form.submit();
					}
				}
			});
		},

		bindings: function() {
			var	$wrapper = $('.address-switch');

			$wrapper.find('.list-buttons a').on({
				click: function(e) {
					store.addressSwitch.getAddress($(this));
					e.preventDefault();
				}
			});

			$wrapper.find('.button-add').on({
				click: function(e) {
					store.addressSwitch.getAddress($(this), true);
					e.preventDefault();
				}
			});

			$wrapper.closest('form').on({
				click: function(e) {
					var	url = $(this).attr('href') + $(this).closest('form').find('.list-buttons .active a').data('param');

					document.location.href = url;

					e.preventDefault();
				}
			}, '.button-remove');

			$wrapper.closest('form').on({
				submit: function(e) {
					var	action = $(this).find('.address-switch').data('action');

					if ($(this).find('.list-buttons .active').length)
						action += $(this).find('.list-buttons .active a').data('param');
					else
						action += $(this).find('.button-add').data('param');

					if (!$(this).data('checked')) {
						$(this).attr('action', action);
	
						store.addressSwitch.validate($(this));
						e.preventDefault();
					}
				}
			});
		},

		init: function() {
			var	$wrapper = $('.address-switch');

			if ($wrapper.length) {
				store.addressSwitch.build();
				store.addressSwitch.bindings();
			}
		}
	};
}(jQuery, this, this.document));
