;(function($, window, document, undefined) {
	'use strict';

	store.form = {
		masks: function() {
			$('.mask-date').mask('99/99/9999');
			$('.mask-postal').mask('99999-999');
			$('.mask-cpf').mask('999.999.999-99');

			$('.mask-phone').on({
				focusin: function() {
					$(this).mask('(99) 99999999?9');
				},

				focusout: function() {
					var	phone = $(this).val().replace(/\D/g, '');

					if (phone.length > 10)
						$(this).mask('(99) 99999-999?9');
					else
						$(this).mask('(99) 9999-9999');
				}
			});
		},

		datepicker: function() {
			var	$wrapper = $('.datepicker');

			if ($wrapper.length) {
				$wrapper.appendDtpicker({
					autodateOnStart: false,
					todayButton: false,
					locale: 'pt',
					dateFormat: 'DD/MM/YYYY',
					closeOnSelected: true,
					dateOnly: true,
				});
			}
		},

		datetimepicker: function() {
			var	$wrapper = $('.datetimepicker, .field-delivery input');

			if ($wrapper.length) {
				$wrapper.appendDtpicker({
					autodateOnStart: false,
					todayButton: false,
					locale: 'pt',
					dateFormat: 'DD/MM/YYYY hh:mm',
					minuteInterval: 30,
					closeOnSelected: true
				});
			}
		},

		zipcodeCheck: function($input) {
			var $fields_update = $input.closest('form').find('[name="endereco"], [name="bairro"], [name="cidade"], [name="estado"]'),
				$button_zipcode_check = $input.closest('label').find('button'),
				$alert = $input.next('.errorList'),
				zipcode = $input.val().replace(/\D/g, ''),
				class_loading = 'field-loading';

			function fiedsPopulate(data) {
				$('[name="cidade"]').val(data.localidade);
				$('[name="estado"]').val(data.uf);
				$('[name="bairro"]').val(data.bairro);
				$('[name="endereco"]').val(data.logradouro);
				$('[name="numero"]').focus();
			}

			if ($alert.length) {
				$alert.remove();
			}

			if (zipcode !== '') {
				$.ajax({
					url: 'http://cep.correiocontrol.com.br/' + zipcode + '.json',

					beforeSend: function() {
						$input.addClass('field-loading');
						$button_zipcode_check.attr('disabled', 'disabled');
					},

					error: function() {
						$input.after('<ul class="errorList"><li>CEP Inválido! Tente novamente.</li></ul>').focus();
						$fields_update.val('');
						$input.removeClass(class_loading);
						$button_zipcode_check.removeAttr('disabled');
					},

					success: function(data) {
						if ($input.data('deliveryAddress')) {
							$.ajax({
								dataType: 'json',
								url: $input.data('deliveryAddress') + '?estado=' + data.uf + '&cidade=' + data.localidade + '&bairro=' + data.bairro,

								error: function() {
									$input.after('<ul class="errorList"><li>Ops! Algo não deu certo... :(</li></ul>').focus();
									$fields_update.val('');
								},

								success: function(delivery) {
									if (delivery.error) {
										if (delivery.error === 1) {
											$input.after('<ul class="errorList"><li>Ops! Algo não deu certo... :(</li></ul>');
										} else {
											$input.after('<ul class="errorList"><li>' + delivery.error + '</li></ul>');
										}

										$input.focus();
										$fields_update.val('');
									} else {
										fiedsPopulate(data);
									}
								},

								complete: function() {
									$input.removeClass(class_loading);
									$button_zipcode_check.removeAttr('disabled');
								}
							});
						} else {
							fiedsPopulate(data);
						}
					},

					complete: function() {
						if (!$input.data('deliveryAddress')) {
							$input.removeClass(class_loading);
							$button_zipcode_check.removeAttr('disabled');
						}
					}
				});
			} else {
				$fields_update.val('');
			}
		},

		shipping: function() {
			var	$wrapper = $('.form-shipping');

			if ($wrapper.length) {
				$wrapper.find('[name="shipping"]').on({
					change: function() {
						var	url = $(this).val();

						document.location.href = url;
					}
				});
			}
		},

		delivery: function() {
			var	$wrapper = $('.form-delivery'),
				$field_day = $wrapper.find('[name="delivery_day"]'),
				$field_hour = $wrapper.find('[name="delivery_hour"]'),
				$form_shipping = $('.form-payment');

			// Day change
			function dayChange() {
				var	$zero = $field_hour.find('option:first-child'),
					url = $field_day.data('getHour'),
					val = $field_day.val();

				$form_shipping.find('[name="delivery_day"]').val(val);

				$.ajax({
					type: 'post',
					url: url,
					data: {
						day: val
					},

					beforeSend: function() {
						$zero.text('Aguarde...');
					},

					error: function() {
						alert('Ops! Algo deu errado... :(');
					},

					success: function(data) {
						$field_hour.find('option:not(:first-child)').remove();
						$field_hour.append(data).change();
					},

					complete: function() {
						$zero.text('Selecione');
					}
				});
			}

			if ($wrapper.length) {

				// Build
				if ($field_day.val()) {
					dayChange();
				}

				// Bindings
				$field_day.on({
					change: function() {
						dayChange();
					}
				});

				$field_hour.on({
					change: function() {
						var	val = $(this).val();

						$form_shipping.find('[name="delivery_hour"]').val(val);
					}
				});
			}
		},

		payment: function() {
			var	$wrapper = $('.form-payment');

			if ($wrapper.length) {
				$wrapper.prepend('<div class="fields-get" />');

				$wrapper.on({
					submit: function(e) {
						if (!$(this).data('checked')) {
							store.checkout.pay($(this));
							e.preventDefault();
						}
					}
				});
			}
		},

		bindings: function() {

			// Zipcode check
			$('.button-zipcode-check').on({
				click: function() {
					store.form.zipcodeCheck($('[data-zipcode-check]'));
				}
			});

			$('.button-submit').on({
				click: function(e) {
					if ($(this).attr('disabled') === undefined)
						$(this).closest('form').submit();

					e.preventDefault();
				}
			});
		},

		init: function() {
			var	that = this;

			that.masks();
			that.datepicker();
			that.datetimepicker();
			that.shipping();
			that.delivery();
			that.payment();
			that.bindings();
		}
	};
}(jQuery, this, this.document));
