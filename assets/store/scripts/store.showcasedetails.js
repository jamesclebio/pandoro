;(function($, window, document, undefined) {
	'use strict';

	store.showcaseDetails = {
		available: function() {
			var	$wrapper = $('.showcase-details'),
				$list_field = $wrapper.find('.list-field'),
				$action = $wrapper.find('.details .action'),
				$container = $wrapper.find('.available'),
				unavailable = 0,
				field_value;

			function setYes(num) {
				$container.removeClass('available-no').addClass('available-yes').html('Disponível em estoque: <strong>' + num + '</strong>');

				if (num > 1)
					$container.find('strong').append(' itens');
				else if (num == 1)
					$container.find('strong').append(' item');
				else
					$container.text('Disponível em estoque!');
			}

			function setNo() {
				$container.removeClass('available-yes').addClass('available-no').text('Produto indisponível!');
			}

			if ($list_field.length) {
				$list_field.find('a').each(function() {
					var	stock = $(this).data('stock');

					if (stock === 0)
						$(this).closest('li').addClass('disabled');
					else
						unavailable++;
				});

				unavailable = (unavailable !== 0) ? false : true;

				if (unavailable)
					setNo();
				else {
					field_value = $list_field.find('.active a').data('stock');

					if (field_value !== null) {
						if (field_value > 0 || field_value == 'x')
							setYes(field_value);
						else
							setNo();
					}
				}
			}

			if ($wrapper.find('.available-no').length)
				$action.hide();
			else
				$action.show();
		},

		zoom: function() {
			var	$wrapper = $('.showcase-details');

			$wrapper.find('.images .medium img').elevateZoom({
				zoomWindowWidth: 530,
				zoomWindowHeight: 471,
				zoomWindowOffetx: 38,
				borderSize: 1,
				loadingIcon: true
			});
		},

		bindings: function() {
			var	$wrapper = $('.showcase-details');

			$wrapper.find('.images .list-thumbs a').on({
				click: function(e) {
					var	$target = $wrapper.find('.images .medium'),
						url = $(this).attr('href'),
						large = $(this).data('large');

					$(this).closest('ul').find('li').removeClass('active');
					$(this).closest('li').addClass('active');

					$.ajax({
						url: url,

						beforeSend: function() {
							$target.append('<div class="loader" />');
							$target.find('.loader').show();
						},

						error: function() {
							$target.find('.loader').fadeOut(100, function() {
								$(this).remove();
							});
						},

						success: function(data) {
							var	$image = $target.find('img'),
								$zoom_container = $('.zoomWindowContainer div'),
								animation_speed = 50;
							
							$image.fadeOut(animation_speed, function() {
								$image.attr({
									'src': url,
									'data-zoom-image': large
								}).data('data-zoom-image', large);

								$target.find('.loader').fadeOut(animation_speed, function() {
									$image.fadeIn(animation_speed);
									$(this).remove();
								});
							});

							$.ajax({
								url: large,

								beforeSend: function() {
									$zoom_container.append('<div class="loader" />');
								},

								success: function() {
									$zoom_container.css('background-image', 'url(' + large + ')').find('.loader').remove();
								}
							});
						}
					});

					e.preventDefault();
				}
			});

			$wrapper.find('.details .list-field a').on({
				click: function(e) {
					var	$button_cart = $wrapper.find('.details .button-cart'),
						$alert_error = $(this).closest('ul').next('.errorList'),
						val = $(this).data('value');
						

					if (!$(this).closest('.disabled').length) {
						$(this).closest('ul').find('li').removeClass('active');
						$(this).closest('li').addClass('active');

						if ($alert_error.length)
							$alert_error.remove();

						store.showcaseDetails.available();
					}

					e.preventDefault();
				}
			});

			$wrapper.find('.details .button-cart').on({
				click: function(e) {
					var	$list_field = $wrapper.find('.list-field'),
						action = $(this).data('action'),
						alert_template = '<ul class="errorList"><li>É necessário selecionar um tamanho para continuar.</li></ul>',
						field_value;

					if ($list_field.length) {
						field_value = $list_field.find('.active a').data('value');

						if (field_value === null) {
							if (!$list_field.next('.errorList').length)
								$list_field.after(alert_template);
						} else
							document.location.href = action + field_value;
					} else
						document.location.href = action;

					e.preventDefault();
				}
			});
		},

		init: function() {
			var	$wrapper = $('.showcase-details');

			if ($wrapper.length) {
				store.showcaseDetails.available();
				store.showcaseDetails.zoom();
				store.showcaseDetails.bindings();
			}
		}
	};
}(jQuery, this, this.document));
