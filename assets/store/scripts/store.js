window.store = {
	about: {
		name: 'Store',
		version: '1.0.0',
		release: 'January, 2014',
		author: 'James Clébio <jamesclebio@gmail.com>',
		license: 'MIT'
	}
};
