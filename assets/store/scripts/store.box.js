;(function($, window, document, undefined) {
	'use strict';

	store.box = {
		build: function() {
			var	$box_toggle = $('.box-toggle');

			if ($box_toggle.length) {
				$box_toggle.each(function() {
					var	box_title = $(this).data('boxTitle');

					if (box_title !== undefined) {
						$(this).prepend('<div class="box-toggle-trigger"><a href="#"></a></div>');

						if ($(this).hasClass('box-toggle-open'))
							$(this).find('.box-toggle-trigger a').text('Ocultar ' + box_title);
						else
							$(this).find('.box-toggle-trigger a').text('Mostrar ' + box_title);
					}
				});
			}
		},

		bindings: function() {
			$('.box-toggle').on({
				click: function(e) {
					var	$this = $(this),
						$box_toggle = $(this).closest('.box-toggle'),
						box_title = $box_toggle.data('boxTitle'),
						class_open = 'box-toggle-open';

					$box_toggle.find('.box-toggle-container').fadeToggle(100, function() {
						if ($(this).is(':visible')) {
							$this.text('Ocultar ' + box_title);
							$box_toggle.addClass(class_open);

						} else {
							$this.text('Mostrar ' + box_title);
							$box_toggle.removeClass(class_open);
						}
					});

					e.preventDefault();
				}
			}, '.box-toggle-trigger a');
		},

		init: function() {
			store.box.build();
			store.box.bindings();
		}
	};
}(jQuery, this, this.document));
