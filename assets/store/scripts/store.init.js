;(function($, window, document, undefined) {
	'use strict';

	store.init = function() {
		var	plugins = [
			'alert',
			'dropdown',
			'collapse',
			'box',
			'form',
			'header',
			'nav',
			'showcase',
			'showcaseDetails',
			'addressSwitch',
			'cart',
			'gallery',
			'selectPush',
			'fieldCount',
			'social'
		];

		if (arguments.length) {
			plugins = arguments;
		}

		for (var i in plugins) {
			this[plugins[i]].init();
		}
	};

	store.init();
}(jQuery, this, this.document));

