;(function($, window, document, undefined) {
	'use strict';

	store.slider = {
		build: function() {
			var	$wrapper = $('.slider'),
				progress = false;

			$wrapper.append('<div class="nav"><a href="#" class="prev">prev</a><a href="#" class="next">next</a></div>');

			if ($wrapper.find('.container .item').length > 1) {
				$wrapper.append('<div class="progress"></div>');
				progress = $wrapper.find('.progress');
			}

			$wrapper.find('.container').carouFredSel({
				items: {
					visible: 1,
					minimum: 2
				},
				scroll: {
					fx: 'crossfade'
				},
				auto: {
					timeoutDuration: 4000,
					progress: progress
				},
				prev: $wrapper.find('.nav .prev'),
				next: $wrapper.find('.nav .next')
			});
		},

		init: function() {
			var	$wrapper = $('.slider');

			if ($wrapper.length) {
				store.slider.build();
			}
		}
	};
}(jQuery, this, this.document));
