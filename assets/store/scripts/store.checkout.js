;(function($, window, document, undefined) {
	'use strict';

	store.checkout = {
		pay: function($form) {
			var	$fields_get = $form.find('.fields-get'),
				$button_submit = $form.find('.button-submit'),
				text_button_submit = $button_submit.text();

			function resultError() {
				var	$alert = $('.alert-main'),
					template_alert = '<div class="alert-main alert-main-error"><a href="#" class="close" title="Fechar alerta">x</a><p><strong>Ops! Algo deu errado... :(</strong></p><p>Por favor, tente novamente. Se o problema persistir, tente <a href="#">falar conosco</a> relatando este ocorrido. Teremos prazer em ajudar!</p></div>';

				$form.removeData('checked');
				$fields_get.empty();
				$button_submit.removeAttr('disabled').text(text_button_submit);

				if ($alert.length) {
					$alert.fadeOut(100, function() {
						$(this).remove();
						$('.heading-page').after(template_alert);
					});
				} else
					$('.heading-page').after(template_alert);

				$('html, body').animate({
					scrollTop: $('.heading-page').offset().top - 15
				}, 150);
			}

			$.ajax({
				cache: false,
				type: $form.attr('method'),
				url: $form.attr('action'),
				data: $form.serialize(),

				beforeSend: function() {
					$fields_get.empty();
					$button_submit.attr('disabled', 'disabled').text('Validando Pedido...');
				},

				error: function() {
					resultError();
				},

				success: function(data) {
					var	$action;

					$form.find('.fields-get').append(data);
					$action = $form.find('[name="form-action"]');

					if ($action.val() === '' || $action.val() === undefined)
						resultError();
					else {
						$form.attr('action', $action.val()).data('checked', true);
						$action.remove();
						$form.submit();
					}
				}
			});
		}
	};
}(jQuery, this, this.document));
