;(function($, window, document, undefined) {
	'use strict';

	store.showcase = {
		build: function() {
			var	$wrapper = $('.showcase');

			$wrapper.find('.item').append('<div class="mask" />');

			$('.showcase-carousel').each(function() {
				var $this = $(this);

				$this.css('overflow', 'visible').append('<div class="nav"><a href="#" class="prev">prev</a><a href="#" class="next">next</a></div>');

				$this.find('ul').carouFredSel({
					items: 4,
					auto: false,
					scroll: {
						duration: 1000
					},
					prev: $this.find('.nav .prev'),
					next: $this.find('.nav .next')
				});
			});
		},

		init: function() {
			var	$wrapper = $('.showcase');

			if ($wrapper.length) {
				store.showcase.build();
			}
		}
	};
}(jQuery, this, this.document));
