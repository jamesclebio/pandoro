;(function($, window, document, undefined) {
	'use strict';

	store.alert = {
		wrapper: '.alert-main',
		template: '<div class="alert-main"><a href="#" class="close" title="Fechar alerta">x</a></div>',

		bindings: function() {
			$(document).on({
				click: function(e) {
					$(this).closest(store.alert.wrapper).fadeOut(100, function() {
						$(this).remove();
					});

					e.preventDefault();
				}
			}, store.alert.wrapper + ' .close');
		},

		init: function() {
			store.alert.bindings();
		}
	};
}(jQuery, this, this.document));
