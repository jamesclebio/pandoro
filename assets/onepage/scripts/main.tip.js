;(function($, window, document, undefined) {
	'use strict';

	var	main = onepage;

	main.tip = {
		wrapper: '.tip',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			that.loader($wrapper.data('start'));
		},

		loader: function(url) {
			var	that = this,
				$wrapper = $(that.wrapper),
				loader_class = 'loader',
				loader_template = '<div class="loader"><div><span class="icon-loading-c"></span></div></div>';

			$.ajax({
				type: 'get',
				url: url,

				beforeSend: function() {
					$wrapper.addClass(loader_class).append(loader_template);
				},

				error: function() {
					alert('Ops! Algo deu errado... :(');
				},

				success: function(data) {
					$wrapper.empty().append(data);

					main.init('gallery');
				},

				complete: function() {
					$wrapper.removeClass(loader_class);

					$wrapper.find('.' + loader_class).fadeOut(function() {
						$(this).remove();
					});
				}
			});
		},

		bindings: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			// Previous, Next
			$wrapper.on({
				click: function(e) {
					that.loader($(this).attr('href'));
					e.preventDefault();
				}
			}, '.nav a');

			// Gallery Button
			$wrapper.on({
				click: function(e) {
					$wrapper.find('.gallery li:first-child a').trigger('click');

					e.preventDefault();
				}
			}, '.gallery-button');
		},

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.build();
				that.bindings();
			}
		}
	};
}(jQuery, this, this.document));
