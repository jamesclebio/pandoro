;(function($, window, document, undefined) {
	'use strict';

	var	main = onepage;

	main.scrollto = function(selector) {
		var	$target = $(selector);

		if (!$target.length)
			$target = $('body');

		$('html, body').animate({
			scrollTop: $target.offset().top - 186
		});
	};
}(jQuery, this, this.document));
