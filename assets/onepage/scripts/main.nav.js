;(function($, window, document, undefined) {
	'use strict';

	var	main = onepage;

	main.nav = {
		wrapper: 'a[href*="#!/"]',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper),
				target = (/#!\//.test(window.location.hash)) ? window.location.hash.replace('!/', '') : false;

			if (target)
				main.scrollto(target);
		},

		bindings: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$wrapper.on({
				click: function(e) {
					var	target = $(this).attr('href').replace(/(.+)?#!\//, '#');

					main.scrollto(target);
				}
			});
		},

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.build();
				that.bindings();
			}
		}
	};
}(jQuery, this, this.document));
