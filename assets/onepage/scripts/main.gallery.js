;(function($, window, document, undefined) {
	'use strict';

	var	main = onepage;

	main.gallery = {
		wrapper: '[rel^=gallery]',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$wrapper.prettyPhoto({
				social_tools: false
			});
		},

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.build();
			}
		}
	};
}(jQuery, this, this.document));
