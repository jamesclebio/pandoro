;(function($, window, document, undefined) {
	'use strict';

	var	main = onepage;

	main.billboard = {
		wrapper: '.billboard',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper),
				$container = $wrapper.find('.container'),
				template_prevnext = '<div class="nav"><a href="#" class="prev"></a><a href="#" class="next"></a></div>',
				active_class = 'active';

			$wrapper.append(template_prevnext);
			$container.find('.item:first').addClass(active_class);

			$container.carouFredSel({
				items: {
					visible: 1,
					minimum: 2
				},
				scroll: {
					fx: 'crossfade',

					onBefore: function(data) {
						data.items.old.removeClass(active_class);
					},

					onAfter: function(data) {
						data.items.visible.addClass(active_class);
					}
				},
				auto: {
					timeoutDuration: 4000
				},
				prev: $wrapper.find('.nav .prev'),
				next: $wrapper.find('.nav .next'),
				responsive: true
			});
		},

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.build();
			}
		}
	};
}(jQuery, this, this.document));
