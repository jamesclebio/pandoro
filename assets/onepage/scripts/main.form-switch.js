;(function($, window, document, undefined) {
	'use strict';

	var	main = onepage;

	main.formSwitch = {
		wrapper: '.form-switch',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$wrapper.each(function() {
				var	$container = $($(this).find('.active a').attr('href'));

				if ($container)
					$container.show();
			});
		},

		toggle: function($this) {
			var	that = this,
				$index = $this.closest(that.wrapper),
				$container = $index.next('.form-switch-container'),
				$content = $($this.attr('href')),
				active_class = 'active';

			$container.find('> div').hide();
			$index.find('.active').removeClass(active_class);
			$content.show();
			$this.closest('li').addClass(active_class);
		},

		bindings: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$wrapper.find('a').on({
				click: function(e) {
					that.toggle($(this));
					e.preventDefault();
				}
			});
		},

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.build();
				that.bindings();
			}
		}
	};
}(jQuery, this, this.document));
