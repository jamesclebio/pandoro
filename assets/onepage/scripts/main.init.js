;(function($, window, document, undefined) {
	'use strict';

	var	main = onepage;

	main.init = function() {
		var	plugins = [
			'form',
			'billboard',
			'nav',
			'tip',
			'gallery',
			'formSwitch'
		];

		if (arguments.length)
			plugins = arguments;

		for (var i in plugins)
			this[plugins[i]].init();
	};

	main.init();
}(jQuery, this, this.document));
