module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		project: {
			banner:	'/* Built on <%= grunt.template.today("dddd, mmmm dS, yyyy, h:MM:ss TT") %> */',
			store: {
				scripts: {
					main: [
						'assets/base/scripts/jquery.js',
						'assets/base/scripts/jquery.maskedinput.js',
						'assets/base/scripts/jquery.caroufredsel.js',
						'assets/base/scripts/jquery.prettyphoto.js',
						'assets/base/scripts/jquery.elevatezoom.js',
						'assets/base/scripts/jquery.simple-dtpicker.js',
						'assets/base/scripts/jquery.sociallikes.js',
						'assets/store/scripts/store.js',
						'assets/store/scripts/store.addressSwitch.js',
						'assets/store/scripts/store.alert.js',
						'assets/store/scripts/store.box.js',
						'assets/store/scripts/store.cart.js',
						'assets/store/scripts/store.checkout.js',
						'assets/store/scripts/store.collapse.js',
						'assets/store/scripts/store.form.js',
						'assets/store/scripts/store.header.js',
						'assets/store/scripts/store.nav.js',
						'assets/store/scripts/store.showcase.js',
						'assets/store/scripts/store.showcaseDetails.js',
						'assets/store/scripts/store.dropdown.js',
						'assets/store/scripts/store.select-push.js',
						'assets/store/scripts/store.gallery.js',
						'assets/store/scripts/store.field-count.js',
						'assets/store/scripts/store.social.js',
						'assets/store/scripts/store.init.js'
					],
					init: [
						'assets/base/scripts/modernizr.js',
						'assets/default/scripts/init.js'
					]
				},
				styles: {
					path: 'assets/store/styles',
					compile: '<%= project.store.styles.path %>/**/*.sass'
				}
			},
			onepage: {
				scripts: {
					main: [
						'assets/base/scripts/jquery.js',
						'assets/base/scripts/jquery.maskedinput.js',
						'assets/base/scripts/jquery.caroufredsel.js',
						'assets/base/scripts/jquery.prettyphoto.js',
						'assets/onepage/scripts/main.js',
						'assets/onepage/scripts/main.form.js',
						'assets/onepage/scripts/main.scrollto.js',
						'assets/onepage/scripts/main.billboard.js',
						'assets/onepage/scripts/main.nav.js',
						'assets/onepage/scripts/main.tip.js',
						'assets/onepage/scripts/main.gallery.js',
						'assets/onepage/scripts/main.form-switch.js',
						'assets/onepage/scripts/main.init.js'
					]
				},
				styles: {
					path: 'assets/onepage/styles',
					compile: '<%= project.onepage.styles.path %>/**/*.sass'
				}
			}
		},

		jshint: {
			options: {
				force: true
			},
			store_main: ['assets/store/scripts/**/*', '!assets/store/scripts/init.js'],
			store_init: ['assets/store/scripts/init.js'],
			onepage_main: ['assets/onepage/scripts/**/*']
		},

		concat: {
			options: {
				banner:	'<%= project.banner %>\n'
			},
			store_main: {
				src: ['<%= project.store.scripts.main %>'],
				dest: 'deploy/assets/store/scripts/main-source.js'
			},
			store_init: {
				src: ['<%= project.store.scripts.init %>'],
				dest: 'deploy/assets/store/scripts/init-source.js'
			},
			onepage_main: {
				src: ['<%= project.onepage.scripts.main %>'],
				dest: 'deploy/assets/onepage/scripts/main-source.js'
			}
		},

		uglify: {
			options: {
				banner:	'<%= project.banner %>\n'
			},
			store_main: {
				src: '<%= project.store.scripts.main %>',
				dest: 'deploy/assets/store/scripts/main.js'
			},
			store_init: {
				src: '<%= project.store.scripts.init %>',
				dest: 'deploy/assets/store/scripts/init.js'
			},
			onepage_main: {
				src: '<%= project.onepage.scripts.main %>',
				dest: 'deploy/assets/onepage/scripts/main.js'
			}
		},

		compass: {
			options: {
				banner:	'<%= project.banner %>',
				relativeAssets: true,
				noLineComments: true,
				outputStyle: 'compressed',
				raw: 'preferred_syntax = :sass'
			},
			store: {
				options: {
					sassDir: '<%= project.store.styles.path %>',
					specify: '<%= project.store.styles.compile %>',
					cssDir: 'deploy/assets/store/styles',
					imagesDir: 'deploy/assets/store/images',
					javascriptsDir: 'deploy/assets/store/scripts',
					fontsDir: 'deploy/assets/store/fonts'
				}
			},
			onepage: {
				options: {
					sassDir: '<%= project.onepage.styles.path %>',
					specify: '<%= project.onepage.styles.compile %>',
					cssDir: 'deploy/assets/onepage/styles',
					imagesDir: 'deploy/assets/onepage/images',
					javascriptsDir: 'deploy/assets/onepage/scripts',
					fontsDir: 'deploy/assets/onepage/fonts'
				}
			}
		},

		watch: {
			options: {
				livereload: true
			},
			grunt: {
				files: ['Gruntfile.js'],
				tasks: ['default']
			},
			store_scripts_main: {
				files: ['<%= project.store.scripts.main %>'],
				tasks: ['jshint:store_main', 'uglify:store_main']
			},
			store_scripts_init: {
				files: ['<%= project.store.scripts.init %>'],
				tasks: ['jshint:store_init', 'uglify:store_init']
			},
			store_styles: {
				files: ['<%= project.store.styles.compile %>'],
				tasks: ['compass:store']
			},
			onepage_scripts_main: {
				files: ['<%= project.onepage.scripts.main %>'],
				tasks: ['jshint:onepage_main', 'uglify:onepage_main']
			},
			onepage_styles: {
				files: ['<%= project.onepage.styles.compile %>'],
				tasks: ['compass:onepage']
			},
			app: {
				files: ['deploy/app/**/*']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('default', ['jshint', 'uglify', 'compass']);
};
